#!/bin/bash
#Ce script permet de supprimer tous les fichiers pdf datant de plus d'un jour 
#se trouvant dans le repertoire cite en argument,
#ou par defaut le repertoire courant


#Check if missing argument, the validity of the argument (directory path) and if too much arguments, exit with error code.
if [ "$#" -eq 1 ]; then
	directory=$1
	if [ -d "$1" ]; then
		#Is a adirectory : OK
	else 
		echo "$0 : The path doesn't lead to a directory" > /dev/stderr
		exit 2
	fi
elif [ "$#" -eq 0 ]; then
	directory='./';
else 
	echo "Usage $0 [     [directory_path]]" > /dev/stderr
	exit 1
fi

find -type f -name "*.pdf" -mtime +1 2> /dev/nulll -exec rm {} \;
exit 0


<?php 
$string = '';
// get the files from models
if($export_agreement)
	$filename = "models/internship_agreement.tex";
else $filename = "models/export_internship.tex";
// compile the tex file
$output = shell_exec("/usr/bin/pdflatex -output-directory /var/www/LaTeX_server/upload --interaction batchmode $filename");
// print the output so the person can see any errors
$string .= " <p>
	compilation output: 
	$output
	</p>";
?>
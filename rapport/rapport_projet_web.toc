\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Analyse}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Prototypes des interfaces et vues actuelles}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Vues externes}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Vues internes}{7}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}Diagramme de structure de donn\'ees}{11}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Diagrammes de navigation}{12}{subsection.2.3}
\contentsline {section}{\numberline {3}Organisation du projet}{13}{section.3}
\contentsline {subsection}{\numberline {3.1}Organisation des t\^aches}{13}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Agenda de l'application}{14}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Le board de Trello}{16}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Le partage}{17}{subsection.3.4}
\contentsline {paragraph}{Le logiciel de versionning Git (Bitbucket )}{17}{section*.2}
\contentsline {paragraph}{Le site de gestion de t\^aches Trello }{17}{section*.3}
\contentsline {paragraph}{Le r\'eseau social Facebook }{18}{section*.4}
\contentsline {paragraph}{L'\'editeur de texte \LaTeX }{18}{section*.5}
\contentsline {section}{\numberline {4}\'{E}tat des lieux de l'application}{18}{section.4}
\contentsline {subsection}{\numberline {4.1}G\'en\'eral}{18}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Attention}{18}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Librairies utilis\'ees}{18}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Choix partial}{19}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}Corrections \`a effectuer}{19}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Vue interne}{19}{subsubsection.4.2.1}
\contentsline {paragraph}{teachers\_list\_admin.php}{19}{section*.6}
\contentsline {subsubsection}{\numberline {4.2.2}Vue externe}{19}{subsubsection.4.2.2}
\contentsline {paragraph}{login.php}{19}{section*.7}
\contentsline {subsection}{\numberline {4.3}Am\'eliorations effectu\'ees}{20}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Vues internes}{20}{subsubsection.4.3.1}
\contentsline {paragraph}{internships\_list\_admin.php}{20}{section*.8}
\contentsline {paragraph}{students\_list\_admin.php}{20}{section*.9}
\contentsline {paragraph}{teachers\_list\_admin.php}{20}{section*.10}
\contentsline {paragraph}{contacts\_list\_admin.php}{20}{section*.11}
\contentsline {paragraph}{companies\_list\_admin.php}{21}{section*.12}
\contentsline {paragraph}{supervisor.php}{21}{section*.13}
\contentsline {subsubsection}{\numberline {4.3.2}Vue externe}{21}{subsubsection.4.3.2}
\contentsline {paragraph}{form.php}{21}{section*.14}
\contentsline {subsection}{\numberline {4.4}Id\'ees d'am\'eliorations}{21}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}Vues internes}{21}{subsubsection.4.4.1}
\contentsline {paragraph}{internship\_agreement et export\_internship}{21}{section*.15}
\contentsline {paragraph}{internships\_list\_admin.php}{22}{section*.16}
\contentsline {paragraph}{students\_list\_admin.php}{24}{section*.17}
\contentsline {subsubsection}{\numberline {4.4.2}Vues externes}{24}{subsubsection.4.4.2}
\contentsline {paragraph}{student.php}{24}{section*.18}
\contentsline {paragraph}{form.php}{24}{section*.19}
\contentsline {section}{\numberline {5}Conclusion}{25}{section.5}

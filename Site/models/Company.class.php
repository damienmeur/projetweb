<?php

class Company{
	private $_company_id;
	private $_name;
	private $_address;
	private $_zip;
	private $_locality;
	private $_secretaryship_phone;
	
	public function __construct($_company_id, $_name, $_address, $_zip, $_locality, $_secretaryship_phone){
		$this->_company_id=$_company_id;
		$this->_name=$_name;
		$this->_address=$_address;
		$this->_zip=$_zip;
		$this->_locality=$_locality;
		$this->_secretaryship_phone=$_secretaryship_phone;
	}
	
	public function company_id(){
		return $this->_company_id;
	}
	
	public function name(){
		return $this->_name;
	}
	
	public function address(){
		return $this->_address;
	}
	
	public function zip(){
		return $this->_zip;
	}
	
	public function locality(){
		return $this->_locality;
	}
	
	public function secretaryship_phone(){
		return $this->_secretaryship_phone;
	}
	
	
}

?>
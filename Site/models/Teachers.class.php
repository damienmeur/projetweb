<?php
class Teachers{
	
	private $_email_unique_id;
	private $_is_admin;
	private $_last_name;
	private $_pswd_sha1;
	private $_first_name;


	public function __construct($email_unique_id,$first_name,$last_name,$is_admin,$pswd_sha1){
		
		$this->_email_unique_id = $email_unique_id;
		$this->_is_admin = $is_admin;
		$this->_last_name = $last_name;
		$this->_pswd_sha1 = $pswd_sha1;
		$this->_first_name = $first_name;

	}
	
	public function email(){
		return $this->_email_unique_id;
	}
	public function is_admin(){
		return $this->_is_admin;
	}
	public function name(){
		return $this->_last_name;
	}
	public function password(){
		return $this->_pswd_sha1;
	}
	public function firstname(){
		return $this->_first_name;
	}
}
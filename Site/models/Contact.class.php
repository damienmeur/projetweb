<?php

class Contact{
	private $_intern_contact_id;
	private $_first_name;
	private $_last_name;
	private $_email;
	private $_phone;
	private $_service;
	private $_function;

	public function __construct($_intern_contact_id,$_first_name,$_last_name,$_email,$_phone,$_service,$_function){
		$this->_intern_contact_id=$_intern_contact_id;
		$this->_first_name=$_first_name;
		$this->_last_name=$_last_name;
		$this->_email=$_email;
		$this->_phone=$_phone;
		$this->_service=$_service;
		$this->_function=$_function;
		
	}

	public function id(){
		return $this->_intern_contact_id;
	}
	
	public function first_name(){
		return $this->_first_name;
	}
	
	public function last_name(){
		return $this->_last_name;
	}
	
	public function email(){
		return $this->_email;
	}
	
	public function phone(){
		return $this->_phone;
	}
	
	public function service(){
		return $this->_service;
	}
	
	public function _function(){
		return $this->_function;
	}

}

?>
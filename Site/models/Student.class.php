<?php

class Student{
	private $_registration_id;
	private $_is_active;
	private $_first_name;
	private $_last_name;
	private $_email;
	private $_phone_number;
	private $_pswd_sha1;
	private $_male_female;
	
	
	public function __construct($registration_id, $is_active, $first_name, $last_name, $email, $phone_number, $pswd_sha1, $male_female){
		$this->_registration_id=$registration_id;
		$this->_is_active=$is_active;
		$this->_first_name=$is_active;
		$this->_last_name=$last_name;
		$this->_email=$email;
		$this->_phone_number=$phone_number;
		$this->_pswd_sha1=$pswd_sha1;
		$this->_male_female=$male_female;
	}
	
	public function id(){
		return $this->_registration_id;
	}
	
	public function is_active(){
		return $this->_is_active;
	}
	
	public function first_name(){
		return $this->_first_name;
	}
	
	public function last_name(){
		return $this->_last_name;
	}
	
	public function email(){
		return $this->_email;
	}
	
	public function phone_number(){
		return $this->_phone_number;
	}
	
	public function pswd_sha1(){
		return $this->_pswd_sha1;
	}
	
	public function male_female(){
		return $this->_male_female;
	}
}

?>
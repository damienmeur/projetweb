<?php
class Db {
	private static $instance = null;
	private $_db;
	private function __construct() {
		try {
			$config= require 'models/config.properties.php';
			$this->_db=new PDO('mysql:host='.$config['dBHost'].';dbname='.$config['dBName'].';charset=utf8',$config['dBUser'],$config['dBPasswd']);
			$this->_db->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			$this->_db->setAttribute ( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ );
		} catch ( PDOException $e ) {
			die ( 'Erreur de connexion à la base de données : ' . $e->getMessage () );
		}
	}
	// Pattern Singleton
	public static function getInstance() {
		if (is_null ( self::$instance )) {
			self::$instance = new Db ();
		}
		return self::$instance;
	}
	public function select_teacher() {
		$query = 'SELECT * FROM teachers WHERE is_active!=0';
		$result = $this->_db->query ( $query );
		$row = $result->fetch ();
		$tableau = array ();
		if ($result->rowcount () != 0) {
			while ( $row = $result->fetch () ) {
				$tableau [] = new Teachers ($row->email_unique_id, $row->first_name, $row->last_name, $row->is_admin, $row->pswd_sha1 );
			}
		}
		return $tableau;
	}
	public function select_student() {
		$query = 'SELECT * FROM students';
		$result = $this->_db->query ( $query );
		$row = $result->fetch ();
		$tableau = array ();
		if ($result->rowcount () != 0) {
			while ( $row = $result->fetch () ) {
				$tableau [] = new Student ( $row->registration_id, $row->is_active, $row->first_name, $row->last_name, $row->email, $row->phone_number, $row->pswd_sha1, $row->male_female);
			}
		}
	
		return $tableau;
	}
	
	
	public function insert_student($registration_id, $is_active, $first_name, $last_name, $email, $phone_number = '', $pswd_sha1 = '', $male_female = '') {
		$pswd_sha1 = sha1 ( $pswd_sha1 );
		if ($is_active == 'true') {
			$is_active = 1;
		} elseif ($is_active == 'false') {
			$is_active = 0;
 		}

		$query = 'INSERT INTO students (registration_id,is_active,first_name,last_name,email,phone_number,pswd_sha1, male_female)
				   values (' . $this->_db->quote ( $registration_id ) . ',' . $this->_db->quote ( $is_active ) . ',' . $this->_db->quote ( $first_name ) . ',' . $this->_db->quote ( $last_name ) . ',' . $this->_db->quote ( $email ) . ',' . $this->_db->quote ( $phone_number ) . ',' . $this->_db->quote ( $pswd_sha1 ) . ',' . $this->_db->quote ( $male_female ) . ')';
		$this->_db->prepare ( $query )->execute ();
	}
	
	
	public function insert_teacher($email_unique_id, $first_name, $last_name , $is_admin, $pswd_sha1) {
		$pswd_sha1=sha1($pswd_sha1);
		$query = 'INSERT INTO teachers (email_unique_id,first_name,last_name,is_admin,pswd_sha1,is_active)
				   values ('. $this->_db->quote ( $email_unique_id ) . ',' . $this->_db->quote ( $first_name ) . ',' . $this->_db->quote ( $last_name ) . ',' . $is_admin . ',' . $this->_db->quote ( $pswd_sha1 ) . ', 1' .')';
		$this->_db->prepare ( $query )->execute ();
	}
	
	
		//LOGIN : DAMIEN
	public function update_teacher($email_unique_id, $first_name, $last_name ,$is_admin){

			$first_name=htmlentities($first_name);
			$last_name=htmlentities($last_name);
			$email_unique_id=htmlentities($email_unique_id);
			$query='UPDATE teachers
				SET is_active=1,
					last_name='.$this->_db->quote($last_name).' , 
					first_name='.$this->_db->quote($first_name).' , 
					is_admin='.$is_admin.'
				WHERE email_unique_id='.$this->_db->quote($email_unique_id);
		$this->_db->prepare($query)->execute();


	}
	public function setActiveTeacher($email_unique_id){
		$query='UPDATE teachers
				SET is_active=1
				WHERE email_unique_id='.$this->_db->quote($email_unique_id);
		$this->_db->prepare($query)->execute();
	}
	public function setInactiveTeacher($email_unique_id){
		$query='UPDATE teachers
				SET is_active=0
				WHERE email_unique_id='.$this->_db->quote($email_unique_id);
		$this->_db->prepare($query)->execute();
	}
	public function checkStudentAuth($registrationNumber, $pswd){
		$registrationNumber=htmlentities($registrationNumber);
		$pswd==htmlentities($pswd);
		$pswd=sha1($pswd);
		$query='SELECT registration_id, pswd_sha1 
				FROM students
				WHERE registration_id='.$this->_db->quote($registrationNumber).'
					AND pswd_sha1 ='.$this->_db->quote($pswd).'
					AND NOT(is_active=0)';
		$result=$this->_db->query($query);
		if(!$result ||$result->rowcount()==0) return false;
		else if($result->rowcount()>1) die('Erreur : plus d\'un utilisateur avec ce login !');
		else return true;
	}
	public function updatePasswordTeacher($password, $email_unique_id){
		$password=sha1(htmlentities($password));
		$query='UPDATE teachers
				SET pswd_sha1='.$this->_db->quote($password).'
				WHERE email_unique_id='.$this->_db->quote($email_unique_id);
		$this->_db->prepare($query)->execute();
	}
		public function updatePasswordStudent($password, $registration_id){
		$password=sha1(htmlentities($password));
		$query='UPDATE students
				SET pswd_sha1='.$this->_db->quote($password).'
				WHERE registration_id='.$this->_db->quote($registration_id);
		$this->_db->prepare($query)->execute();
	}
	public function checkTeacherAuth($email, $pswd){
		$email=htmlentities($email);
		$pswd==htmlentities($pswd);
		$pswd=sha1($pswd);
		$query='SELECT email_unique_id, pswd_sha1, is_admin
				FROM teachers
				WHERE email_unique_id='.$this->_db->quote($email).'
					AND pswd_sha1 ='.$this->_db->quote($pswd).'
					AND is_active!=0';
		$result=$this->_db->query($query);
		if(!$result ||$result->rowcount()==0) return 'no';
		elseif($result->rowcount()>1) die('Erreur : plus d\'un utilisateur avec ce login !');
		elseif($result->fetch()->is_admin==0) return 'supervisor';
		else return 'admin';
	}
		//ADMIN :DAMIEN
	public function getTeacherFirstLastName($email){
		$email=htmlentities($email);
		$query='SELECT  first_name, last_name, is_admin
				FROM teachers
				WHERE email_unique_id='.$this->_db->quote($email).'
					AND is_active!=0';
		$result=$this->_db->query($query);
		if(!$result && $result->rowcount()==0) die("Pas de professeur avec cet email dans la bddn");
		$row=$result->fetch();
		$teacherInfo=array();
		$teacherInfo['first_name']=$row->first_name;
		$teacherInfo['last_name']=$row->last_name;
		$teacherInfo['is_admin']=$row->is_admin;
		return $teacherInfo;

	}
	public function getAmountInternshipsTeachers($email){
		$query='SELECT COUNT(IT.internship_id) as amount_it
				FROM internships IT
				WHERE IT.supervisor_email='.$this->_db->quote($email);
		$result=$this->_db->query($query);
		if(!$result && $result->rowcount()==0) return 0;
		return $result->fetch()->amount_it;
	}

	public function selectExportateAndSupervisedInternshipIds(){
		$query='SELECT internship_id
				FROM internships
				WHERE internship_state=\'exporte\' OR internship_state=\'complete\' OR internship_state=\'supervise\'';
		$result=$this->_db->query($query);
		$internships=array();
		if ($result->rowcount() != 0) {
			while ( $row = $result->fetch () ) {
				$internships[]=$row->internship_id;
			}
		}
		return $internships;

	}

	public function getRegistrationId($student_first_name,$student_last_name){
		$query='SELECT registration_id
				FROM students
				WHERE first_name='.$this->_db->quote($student_first_name).'
					AND last_name='.$this->_db->quote($student_last_name);
		$result=$this->_db->query($query);
		return $result->fetch()->registration_id;
	}
	public function getContactFirstLastName($intern_contact_id){
		$query='SELECT  first_name, last_name
				FROM intern_contacts
				WHERE intern_contact_id='.$this->_db->quote($intern_contact_id);
		$result=$this->_db->query($query);
		if(!$result || $result->rowcount()==0) die("Pas de contact avec cet id dans la bddn");
		$row=$result->fetch();
		$contactInfo=array();
		$contactInfo['first_name']=$row->first_name;
		$contactInfo['last_name']=$row->last_name;
		return $contactInfo;
	}

	public function getStudentFirstLastName($registration_id){
		$query='SELECT first_name, last_name
				FROM students
				WHERE registration_id='.$this->_db->quote($registration_id).'
						AND is_active!=0';
		$result=$this->_db->query($query);
		if(!$result || $result->rowcount()==0) die("Pas d'étudiant avec cet id dans la bddn");
		$row=$result->fetch();
		$studentInfo=array();
		$studentInfo['first_name']=$row->first_name;
		$studentInfo['last_name']=$row->last_name;
		return $studentInfo;


	}
	public function getStudentDetails($registration_id){
		$query='SELECT first_name, last_name, phone_number, male_female, email
				FROM students
				WHERE registration_id='.$this->_db->quote($registration_id).'
						AND is_active!=0';
		$result=$this->_db->query($query);
		if(!$result || $result->rowcount()==0) die("Pas d'étudiant avec cet id dans la bddn");
		$row=$result->fetch();
		$studentInfo=array();
		if(is_null($row->email))$studentInfo['email']='/';
		else $studentInfo['email']=$row->email;
		$studentInfo['male_female']=$row->male_female;
		if(is_null($row->male_female)) $studentInfo['male_female']="/";
		$studentInfo['phone_number']=$row->phone_number;
		if($row->phone_number=='') $studentInfo['phone_number']="/";
		$studentInfo['first_name']=$row->first_name;
		$studentInfo['last_name']=$row->last_name;
		return $studentInfo;


	}
	public function getInternshipId($registration_id){
		$query='SELECT internship_id
				FROM internships
				WHERE registration_id='.$this->_db->quote($registration_id);
		$result=$this->_db->query($query);
		if($result->rowcount() != 0){
		$return= $result->fetch()->internship_id;
		}else{
			$return=0;
		}
		return $return; 
	}

	public function getCompanyDetails($company_id){
		$query='SELECT *
				FROM companies
				WHERE company_id='.$this->_db->quote($company_id);
		$result=$this->_db->query($query);
		if(!$result || $result->rowcount()==0) die("Pas de societe avec cet id dans la bddn");
		$row=$result->fetch();
		$companyInfo=array();
		$companyInfo['name']=$row->name;
		$companyInfo['address']=$row->address;
		$companyInfo['locality']=$row->locality;
		$companyInfo['zip']=$row->zip;
		$companyInfo['phone']=$row->secretaryship_phone;
		return $companyInfo;
	}

	public function getContactDetails($intern_contact_id){
		$query='SELECT *
				FROM intern_contacts
				WHERE intern_contact_id='.$this->_db->quote($intern_contact_id);
		$result=$this->_db->query($query);
		if(!$result || $result->rowcount()==0) die("Pas de contact avec cet id dans la bddn");
		$row=$result->fetch();
		$contactInfo=array();
		$contactInfo['first_name']=$row->first_name;
		$contactInfo['last_name']=$row->last_name;
		$contactInfo['email']=$row->email;
		$contactInfo['service']=$row->service;
		$contactInfo['phone']=$row->phone_number;
		$contactInfo['function']=$row->function;
		if(is_null($row->function)) $contactInfo['function']="/";
		if(is_null($row->service)) $contactInfo['service']="/";
		return $contactInfo;


	}

	public function getAllTeachers(){
		$teachers=array();
		$query='SELECT *
				FROM teachers
				WHERE is_active !=0';
		$result=$this->_db->query($query);
		if(!$result or $result->rowcount()==0) return -1;
		while($row = $result->fetch()){
			$teachers[$row->email_unique_id]['teacherNames']=$row->first_name." ".$row->last_name;
		}
		return $teachers;

	}
	public function getAllTeachersCSV(){
		$teachers=array();
		$query='SELECT *
				FROM teachers
				WHERE is_active !=0';
		$result=$this->_db->query($query);
		if(!$result or $result->rowcount()==0) return -1;
		while($row = $result->fetch()){
			if($row->is_admin==1) $is_admin='true';
			else $is_admin='false';
			$teachers[$row->email_unique_id]=$row->email_unique_id.";".$row->last_name.";".$row->first_name.";".$is_admin;
		}
		return $teachers;

	}
	public function getAllTeachersEvenInactive(){
		$teachers=array();
		$query='SELECT *
				FROM teachers';
		$result=$this->_db->query($query);
		if(!$result or $result->rowcount()==0) return -1;
		while($row = $result->fetch()){
			$teachers[$row->email_unique_id]['first_name']=$row->first_name;
			$teachers[$row->email_unique_id]['last_name']=$row->last_name;
			if((int)$row->is_admin==0) $is_admin='false';
			else $is_admin='true';
			$teachers[$row->email_unique_id]['is_admin']=$is_admin;
			$teachers[$row->email_unique_id]['is_active']=(int)$row->is_active;

		}
		return $teachers;

	}

	public function selectInternshipsWithFilters($internship_state,$last_name,$first_name,$company){
		$last_name=strtolower(htmlentities($last_name));
		$first_name=strtolower(htmlentities($first_name));
		$company=strtolower(htmlentities($company));
		$query='';
		if($internship_state=='tous'){
			$query='SELECT IT.internship_id, IT.internship_state, IT.registration_id, CO.name as company_name,
							IT.first_validator_email, IT.second_validator_email, IT.person_to_contact, IT.promoter_id, IT.supervisor_email
					FROM internships IT, students ST, companies CO, teachers FV, teachers SV, teachers SU, intern_contacts IC, intern_contacts PRO
					WHERE lower(ST.last_name) LIKE \'%'.$last_name.'%\'
						 AND lower(ST.first_name) LIKE \'%'.$first_name.'%\'
						 AND lower(CO.name) LIKE \'%'.$company.'%\'
						 AND IT.company_id=CO.company_id
						 AND (IT.registration_id=ST.registration_id OR IT.registration_id IS NULL)
						 AND (IT.first_validator_email=FV.email_unique_id OR IT.first_validator_email IS NULL)
						 AND (IT.second_validator_email=SV.email_unique_id OR IT.second_validator_email IS NULL)
						 AND (IT.person_to_contact=IC.intern_contact_id OR IT.person_to_contact IS NULL)
						 AND (IT.promoter_id=PRO.intern_contact_id OR IT.promoter_id IS NULL)
						 AND (IT.supervisor_email=SU.email_unique_id OR IT.supervisor_email IS NULL)
					GROUP BY IT.internship_id';
		}
		else{
			$query='SELECT IT.internship_id, IT.internship_state, IT.registration_id, CO.name as company_name,
							IT.first_validator_email, IT.second_validator_email, IT.person_to_contact, IT.promoter_id, IT.supervisor_email
					FROM internships IT, students ST, companies CO, teachers FV, teachers SV, teachers SU, intern_contacts IC, intern_contacts PRO
					WHERE lower(ST.last_name) LIKE \'%'.$last_name.'%\'
						 AND lower(ST.first_name) LIKE \'%'.$first_name.'%\'
						 AND lower(CO.name) LIKE \'%'.$company.'%\'
						 AND IT.company_id=CO.company_id
						 AND (IT.registration_id=ST.registration_id OR IT.registration_id IS NULL)
						 AND (IT.first_validator_email=FV.email_unique_id OR IT.first_validator_email IS NULL)
						 AND (IT.second_validator_email=SV.email_unique_id OR IT.second_validator_email IS NULL)
						 AND (IT.person_to_contact=IC.intern_contact_id OR IT.person_to_contact IS NULL)
						 AND (IT.promoter_id=PRO.intern_contact_id OR IT.promoter_id IS NULL)
						 AND (IT.supervisor_email=SU.email_unique_id OR IT.supervisor_email IS NULL)
						 AND IT.internship_state=\''.$internship_state.'\'		 
					GROUP BY IT.internship_id';
			
		}
		$result=$this->_db->query($query);
		$internships=array();
		if ($result->rowcount() != 0) {
			while ( $row = $result->fetch () ) {
				$internship_state= self::convertInternshipStateForDislay($row->internship_state);
				$internships[$row->internship_id]['internship_id'] =$row->internship_id ;
				$internships[$row->internship_id]['internship_state'] =$internship_state ;
				$internships[$row->internship_id]['company_name'] =$row->company_name ;
			
				if(is_null($row->registration_id)){
					$internships[$row->internship_id]['first_name_student'] ="/";
					$internships[$row->internship_id]['last_name_student'] ="/";
				}
				else{
					$studentInfo=$this->getStudentFirstLastName($row->registration_id);
					$internships[$row->internship_id]['first_name_student'] =$studentInfo['first_name'];
					$internships[$row->internship_id]['last_name_student'] =$studentInfo['last_name'] ;

				}
				
				if(is_null($row->first_validator_email)){
					$internships[$row->internship_id]['first_validator']="/";
				}
				else{
					$internships[$row->internship_id]['first_validator']=$this->getTeacherFirstLastName($row->first_validator_email)['first_name']." ".$this->getTeacherFirstLastName($row->first_validator_email)['last_name'];
				}
				if(is_null($row->second_validator_email)){
					$internships[$row->internship_id]['second_validator']="/";
				}
				else{
					$internships[$row->internship_id]['second_validator']=$this->getTeacherFirstLastName($row->second_validator_email)['first_name']." ".$this->getTeacherFirstLastName($row->second_validator_email)['last_name'];
				}
				if(is_null($row->supervisor_email)){
					$internships[$row->internship_id]['supervisor']="/";
				}
				else{
					$internships[$row->internship_id]['supervisor']=$this->getTeacherFirstLastName($row->supervisor_email)['first_name']." ".$this->getTeacherFirstLastName($row->supervisor_email)['last_name'];
					$internships[$row->internship_id]['supervisor_email']=$row->supervisor_email;
				}
				if(is_null($row->promoter_id)){
					$internships[$row->internship_id]['promoter']="/";
				}
				else{
					$internships[$row->internship_id]['promoter']=$this->getContactFirstLastName($row->promoter_id)['first_name']." ".$this->getContactFirstLastName($row->promoter_id)['last_name'];
				}
				if(is_null($row->person_to_contact)){
					$internships[$row->internship_id]['intern_contact']="/";
				}
				else{
					$internships[$row->internship_id]['intern_contact']=$this->getContactFirstLastName($row->person_to_contact)['first_name']." ".$this->getContactFirstLastName($row->person_to_contact)['last_name'];
				}
			}
		}
		if(!empty($_POST))unset($_POST);
		if(!empty($_GET))unset($_GET);
		return $internships;

	}
	public function getInternshipDetails($internship_id){
		$query='SELECT DISTINCT IT.*
				FROM internships IT
				WHERE IT.internship_id='.$internship_id;

		$result=$this->_db->query($query);
		$details=array();
		if ($result->rowcount() != 0) {
			while ( $row = $result->fetch () ) {
				$internship_state= self::convertInternshipStateForDislay($row->internship_state);
				$details['internship_state'] =$internship_state ;
				$companyInfo=$this->getCompanyDetails($row->company_id);
				$details['company_name'] =$companyInfo['name'] ;
				$details['company_address'] =$companyInfo['address'] ;
				$details['company_locality'] =$companyInfo['locality'];
				$details['company_zip'] =$companyInfo['zip'];
				$details['secretaryship_phone'] = $companyInfo['phone'];
			
				if(is_null($row->registration_id)){
					$details['first_name_student'] ="/";
					$details['last_name_student'] ="/";
					$details['student_email']="/";
					$details['student_phone']="/";
				}
				else{
					$studentInfo=$this->getStudentDetails($row->registration_id);
					$details['first_name_student'] =$studentInfo['first_name'];
					$details['last_name_student'] =$studentInfo['last_name'] ;
					$details['student_email']=$studentInfo['email'];
					$details['student_phone']=$studentInfo['phone_number'];
				}
				
				if(is_null($row->promoter_id)){
					$details['promoter_first_name']="/";
					$details['promoter_last_name']="/";
					$details['promoter_service']="/";
					$details['promoter_phone']="/";
					$details['promoter_email']="/";
				}
				else{
					$promoterInfo=$this->getContactDetails($row->promoter_id);
					$details['promoter_first_name']=$promoterInfo['first_name'];
					$details['promoter_last_name']=$promoterInfo['last_name'];
					$details['promoter_service']=$promoterInfo['service'];
					$details['promoter_phone']=$promoterInfo['phone'];
					$details['promoter_email']=$promoterInfo['email'];
				}
				if(is_null($row->person_to_contact)){
					$details['ic_first_name']="/";
					$details['ic_last_name']="/";
					$details['ic_service']="/";
					$details['ic_phone_number']="/";
					$details['ic_email']="/";
					$details['ic_function']="/";
				}
				else{
					$contactInfo=$this->getContactDetails($row->person_to_contact);
					$details['ic_first_name']=$contactInfo['first_name'];
					$details['ic_last_name']=$contactInfo['last_name'];
					$details['ic_service']=$contactInfo['service'];
					$details['ic_function']=$contactInfo['function'];
					$details['ic_phone_number']=$contactInfo['phone'];
					$details['ic_email']=$contactInfo['email'];
				}
				if(is_null($row->description)){
					$details['description']="/";
				}
				else{
					$details['description']=$row->description;
				}
				if(is_null($row->remarks)){
					$details['remarks']="/";
				}
				else{
					$details['remarks']=$row->remarks;
				}
				if(is_null($row->objectives)){
					$details['objectives']="/";
				}
				else{
					$details['objectives']=$row->objectives;
				}
				if(is_null($row->work_environment)){
					$details['work_environment']="/";
				}
				else{
					$details['work_environment']=$row->work_environment;
				}
				if(is_null($row->intership_origin)){
					$details['internship_origin']="/";
				}
				else{
					$details['internship_origin']=$row->intership_origin;
				}
				
			}
		}
		return $details;
	}

	public function getInternshipWithStudent($registration_id){
		$query='SELECT IT.internship_state
					FROM internships IT, students ST
					WHERE IT.registration_id='.$this->_db->quote($registration_id).'
						AND IT.registration_id=ST.registration_id';

		$result=$this->_db->query($query);
		$row=$result->fetch();
		if($result->rowcount()==0) return 'noMatchingInternship';
		elseif($result->rowcount()==1) return $row->internship_state;
	}
	
	public function selectStudentsWithFilters($internship_state,$first_name,$last_name){
		$last_name=strtolower(htmlentities($last_name));
		$first_name=strtolower(htmlentities($first_name));
		$query='';
		if($internship_state=='tous'){
			$query='SELECT DISTINCT ST.*
					FROM students ST
					left outer join internships IT on ST.registration_id=IT.registration_id
					WHERE lower(ST.last_name) LIKE \'%'.$last_name.'%\'
						 AND lower(ST.first_name) LIKE \'%'.$first_name.'%\'
						 AND ST.is_active!=0';	
		}
		
		else{
			$query='SELECT DISTINCT ST.*
				FROM students ST
				left outer join internships IT on ST.registration_id=IT.registration_id
				WHERE lower(ST.last_name) LIKE \'%'.$last_name.'%\'
					 AND lower(ST.first_name) LIKE \'%'.$first_name.'%\'
					 AND ST.is_active!=0
					AND IT.internship_state=\''.$internship_state.'\'';
				
		}
		$result=$this->_db->query($query);
		$students=array();
		if ($result->rowcount() != 0) {
			while ( $row = $result->fetch () ) {
				$students[$row->registration_id]['first_name']=$row->first_name;
				$students[$row->registration_id]['last_name']=$row->last_name;
				$students[$row->registration_id]['email']=$row->email;
				$internship_state=$this->getInternshipWithStudent($row->registration_id);
				$internship_state=self::convertInternshipStateForDislay($internship_state);
				$students[$row->registration_id]['internship_state']=$internship_state;
				if($row->phone_number==''){
					$students[$row->registration_id]['phone']="/";
				}
				else{
					$students[$row->registration_id]['phone']=$row->phone_number;
				}
			
			}
		}
		
		if(!empty($_POST))unset($_POST);
		if(!empty($_GET))unset($_GET);
		return $students;
	}

	public function selectTeachersWithFilters($first_name,$last_name){
		$last_name=strtolower(htmlentities($last_name));
		$first_name=strtolower(htmlentities($first_name));
		$query='SELECT TE.email_unique_id, TE.first_name, TE.last_name,TE.is_admin, count(IT.internship_id) as amount_it
				FROM teachers TE
				left outer join internships IT on IT.supervisor_email=TE.email_unique_id
				WHERE 	 lower(TE.last_name) LIKE \'%'.$last_name.'%\'
					 	AND lower(TE.first_name) LIKE \'%'.$first_name.'%\'
					 	AND TE.is_active!=0
				GROUP BY TE.email_unique_id, TE.first_name, TE.last_name,TE.is_admin';

		$result=$this->_db->query($query);
		$teachers=array();
		if ($result->rowcount() != 0) {
			while ( $row = $result->fetch () ) {
				$teachers[$row->email_unique_id]['first_name']=$row->first_name;
				$teachers[$row->email_unique_id]['last_name']=$row->last_name;
				if($row->is_admin>=1){
					$teachers[$row->email_unique_id]['function']='Responsable de stage';
				}
				else $teachers[$row->email_unique_id]['function']='Superviseur';
				if($row->amount_it==0){
					$teachers[$row->email_unique_id]['internship_id']=null;
					$teachers[$row->email_unique_id]['internship_state']="Pas de stage";
				}
				else{
					if($row->amount_it>1)
						$teachers[$row->email_unique_id]['internship_state']="Supervise ".$row->amount_it." stages";
					else $teachers[$row->email_unique_id]['internship_state']="Supervise ".$row->amount_it." stage";
				}
			}
		}
		if(!empty($_POST))unset($_POST);
		if(!empty($_GET))unset($_GET);
		return $teachers;

	}
	public function selectContactsWithFilters($first_name,$last_name){
		$last_name=strtolower(htmlentities($last_name));
		$first_name=strtolower(htmlentities($first_name));
		$queryContacts='SELECT IC.intern_contact_id, IC.first_name, IC.last_name,IC.email, IC.phone_number, IC.service, IC.function, count(IT.internship_id) as amount_it
				FROM intern_contacts IC
				left outer join internships IT on (IT.person_to_contact=IC.intern_contact_id)
				WHERE 	 lower(IC.last_name) LIKE \'%'.$last_name.'%\'
					 AND lower(IC.first_name) LIKE \'%'.$first_name.'%\'
				GROUP BY IC.intern_contact_id, IC.first_name, IC.last_name,IC.email, IC.phone_number, IC.service, IC.function';
		$queryPromoters='SELECT IC.intern_contact_id, IC.first_name, IC.last_name,IC.email, IC.phone_number, IC.service, IC.function, count(IT.internship_id) as amount_it
				FROM intern_contacts IC
				left outer join internships IT on (IT.promoter_id=IC.intern_contact_id)
				WHERE 	 lower(IC.last_name) LIKE \'%'.$last_name.'%\'
					 AND lower(IC.first_name) LIKE \'%'.$first_name.'%\'
				GROUP BY IC.intern_contact_id, IC.first_name, IC.last_name,IC.email, IC.phone_number, IC.service, IC.function';
		$resultContacts=$this->_db->query($queryContacts);
		$resultPromoters=$this->_db->query($queryPromoters);
		$contacts=array();
		if ($resultContacts->rowcount() != 0) {
			while ( $row = $resultContacts->fetch () ) {
				$contacts[$row->intern_contact_id]['is_promoter']='Contact';
				$contacts[$row->intern_contact_id]['first_name']=$row->first_name;
				$contacts[$row->intern_contact_id]['last_name']=$row->last_name;
				$contacts[$row->intern_contact_id]['phone']=$row->phone_number;
				$contacts[$row->intern_contact_id]['email']=$row->email;
				if(is_null($row->service)){
					$contacts[$row->intern_contact_id]['service']="/";
				}
				else{
					$contacts[$row->intern_contact_id]['service']=$row->service;
				}
				if(is_null($row->function)){
					$contacts[$row->intern_contact_id]['function']="/";
				}
				else{
					$contacts[$row->intern_contact_id]['function']=$row->function;
				}
				if($row->amount_it==0){
					$contacts[$row->intern_contact_id]['internship_id']=null;
					$contacts[$row->intern_contact_id]['internship_state']="Pas de stage";
				}
				else{
					if($row->amount_it==1)
						$contacts[$row->intern_contact_id]['internship_state']="1 stage";
					else $contacts[$row->intern_contact_id]['internship_state']=$row->amount_it." stages";
				}
			}
		}
		if ($resultPromoters->rowcount() != 0) {
			while ( $row = $resultPromoters->fetch () ) {
				$contacts[$row->intern_contact_id]['is_promoter']='Promoteur';
				$contacts[$row->intern_contact_id]['first_name']=$row->first_name;
				$contacts[$row->intern_contact_id]['last_name']=$row->last_name;
				$contacts[$row->intern_contact_id]['phone']=$row->phone_number;
				$contacts[$row->intern_contact_id]['email']=$row->email;
				if(is_null($row->service)){
					$contacts[$row->intern_contact_id]['service']="/";
				}
				else{
					$contacts[$row->intern_contact_id]['service']=$row->service;
				}
				if(is_null($row->function)){
					$contacts[$row->intern_contact_id]['function']="/";
				}
				else{
					$contacts[$row->intern_contact_id]['function']=$row->function;
				}
				if($row->amount_it==0){
					$contacts[$row->intern_contact_id]['internship_id']=null;
					$contacts[$row->intern_contact_id]['internship_state']="Pas de stage";
				}
				else{
					if($row->amount_it==1)
						$contacts[$row->intern_contact_id]['internship_state']="1 stage";
					else $contacts[$row->intern_contact_id]['internship_state']=$row->amount_it." stages";
				}
			}
		}
		if(!empty($_POST))unset($_POST);
		if(!empty($_GET))unset($_GET);
		return $contacts;
	}

	public function selectCompaniesWithFilters($company_name){
		$company_name=strtolower(htmlentities($company_name));
		$query='SELECT CO.company_id, CO.name, CO.address,CO.zip, CO.locality, CO.secretaryship_phone, count(IT.internship_id) as amount_it
				FROM companies CO
				left outer join internships IT on IT.company_id=CO.company_id
				WHERE 	 lower(CO.name) LIKE \'%'.$company_name.'%\'
				GROUP BY CO.company_id, CO.name, CO.address,CO.zip, CO.locality, CO.secretaryship_phone';
		$result=$this->_db->query($query);
		$companies=array();
		if ($result->rowcount() != 0) {
			while ( $row = $result->fetch () ) {
				$companies[$row->company_id]['company_name']=$row->name;
				$companies[$row->company_id]['address']=$row->address;
				$companies[$row->company_id]['zip']=$row->zip;
				$companies[$row->company_id]['locality']=$row->locality;
				$companies[$row->company_id]['phone']=$row->secretaryship_phone;
				if($row->amount_it==0){
					$companies[$row->company_id]['internship_id']=null;
					$companies[$row->company_id]['internship_state']="Pas de stage";
				}
				else{
					if($row->amount_it==1)
						$companies[$row->company_id]['internship_state']="1 stage";
					else $companies[$row->company_id]['internship_state']=$row->amount_it." stages";
				}
			}
		}
		if(!empty($_POST))unset($_POST);
		if(!empty($_GET))unset($_GET);
		return $companies;
	}
	public function selectInternshipsWithFiltersSupervisor($internship_state,$last_name,$first_name,$company, $supervisor_email){
		$last_name=strtolower(htmlentities($last_name));
		$first_name=strtolower(htmlentities($first_name));
		$company=strtolower(htmlentities($company));
		$query='';
		if($internship_state=='tous'){
			$query='SELECT IT.internship_id, IT.internship_state, IT.registration_id, CO.name as company_name,
							IT.first_validator_email, IT.second_validator_email, IT.person_to_contact, IT.promoter_id, IT.supervisor_email
					FROM internships IT, students ST, companies CO, teachers FV, teachers SV, teachers SU, intern_contacts IC, intern_contacts PRO
					WHERE lower(ST.last_name) LIKE \'%'.$last_name.'%\'
						 AND lower(ST.first_name) LIKE \'%'.$first_name.'%\'
						 AND lower(CO.name) LIKE \'%'.$company.'%\'
						 AND IT.company_id=CO.company_id
						 AND IT.registration_id=ST.registration_id
						 AND (IT.first_validator_email=FV.email_unique_id OR IT.first_validator_email IS NULL)
						 AND (IT.second_validator_email=SV.email_unique_id OR IT.second_validator_email IS NULL)
						 AND (IT.person_to_contact=IC.intern_contact_id OR IT.person_to_contact IS NULL)
						 AND (IT.promoter_id=PRO.intern_contact_id OR IT.promoter_id IS NULL)
						 AND IT.supervisor_email='.$this->_db->quote($supervisor_email).'
					GROUP BY IT.internship_id';
		}
		elseif($internship_state=='supervise' or $internship_state=='exporte') {
			$query='SELECT IT.internship_id, IT.internship_state, IT.registration_id, CO.name as company_name,
							IT.first_validator_email, IT.second_validator_email, IT.person_to_contact, IT.promoter_id
					FROM internships IT, students ST, companies CO, teachers FV, teachers SV, teachers SU, intern_contacts IC, intern_contacts PRO
					WHERE lower(ST.last_name) LIKE \'%'.$last_name.'%\'
						 AND lower(ST.first_name) LIKE \'%'.$first_name.'%\'
						 AND lower(CO.name) LIKE \'%'.$company.'%\'
						 AND IT.company_id=CO.company_id
						 AND IT.registration_id=ST.registration_id
						 AND (IT.first_validator_email=FV.email_unique_id OR IT.first_validator_email IS NULL)
						 AND (IT.second_validator_email=SV.email_unique_id OR IT.second_validator_email IS NULL)
						 AND (IT.person_to_contact=IC.intern_contact_id OR IT.person_to_contact IS NULL)
						 AND (IT.promoter_id=IC.intern_contact_id OR IT.promoter_id IS NULL)
						  AND IT.supervisor_email='.$this->_db->quote($supervisor_email).'
						 AND IT.internship_state=\''.$internship_state.'\'
			GROUP BY IT.internship_id';
					 
		}

		$result=$this->_db->query($query);
		$internships=array();
		if ($result->rowcount() != 0) {
			while ( $row = $result->fetch () ) {
				$internship_state= self::convertInternshipStateForDislay($row->internship_state);
				$internships[$row->internship_id]['internship_state'] =$internship_state ;
				$internships[$row->internship_id]['company_name'] =$row->company_name ;
				if(is_null($row->registration_id)){
					$internships[$row->internship_id]['first_name_student'] ="/";
					$internships[$row->internship_id]['last_name_student'] ="/";
				}
				else{
					$studentInfo=$this->getStudentFirstLastName($row->registration_id);
					$internships[$row->internship_id]['first_name_student'] =$studentInfo['first_name'];
					$internships[$row->internship_id]['last_name_student'] =$studentInfo['last_name'] ;

				}
				if(is_null($row->first_validator_email)){
					$internships[$row->internship_id]['first_validator']="/";
				}
				else{

					$internships[$row->internship_id]['first_validator']=$this->getTeacherFirstLastName($row->first_validator_email)['first_name']." ".$this->getTeacherFirstLastName($row->first_validator_email)['last_name'];
				}
				if(is_null($row->second_validator_email)){
					$internships[$row->internship_id]['second_validator']="/";
				}
				else{
					$internships[$row->internship_id]['second_validator']=$this->getTeacherFirstLastName($row->second_validator_email)['first_name']." ".$this->getTeacherFirstLastName($row->second_validator_email)['last_name'];
				}
				if(is_null($row->promoter_id)){
					$internships[$row->internship_id]['promoter']="/";
				}
				else{
					$internships[$row->internship_id]['promoter']=$this->getContactFirstLastName($row->promoter_id)['first_name']." ".$this->getContactFirstLastName($row->promoter_id)['last_name'];
				}
				if(is_null($row->person_to_contact)){
					$internships[$row->internship_id]['intern_contact']="/";
				}
				else{
					$internships[$row->internship_id]['intern_contact']=$this->getContactFirstLastName($row->person_to_contact)['first_name']." ".$this->getContactFirstLastName($row->person_to_contact)['last_name'];
				}
			}
		}
		if(!empty($_POST))unset($_POST);
		if(!empty($_GET))unset($_GET);
		return $internships;

	}
	public function validateCompanyAsk($internship_id, $currentEmail){
		$query='UPDATE internships
				SET internship_state=\'libre\', second_validator_email='.$this->_db->quote($currentEmail).' 
				WHERE internship_id='.$this->_db->quote($internship_id);
		$this->_db->prepare($query)->execute();
	}
	public function preValidate($internship_id, $currentEmail){
		$query='UPDATE internships
				SET  first_validator_email='.$this->_db->quote($currentEmail).' 
				WHERE internship_id='.$internship_id;
		$this->_db->prepare($query)->execute();
	}
	public function validateStudentProposition($internship_id,$currentEmail){
		$query='UPDATE internships
				SET internship_state=\'attribue\', second_validator_email='.$this->_db->quote($currentEmail).' 
				WHERE internship_id='.$this->_db->quote($internship_id);
		$this->_db->prepare($query)->execute();
	}
	public function isPrevalidate($internship_id, $currentEmail){
		$query='SELECT IT.first_validator_email
				FROM internships IT
				WHERE IT.internship_id='.$this->_db->quote($internship_id);
		$result=$this->_db->query($query);
		if(!$result or $result->rowcount() == 0) return "canBePrevalidate";
		$row=$result->fetch();
		$fv=$row->first_validator_email;
		if(is_null($fv)){
			return "canBePrevalidate";

		} 
		elseif($fv!=$currentEmail){
			return "canBeValidate";
		} 
	}
	public function setExportate($internship_id){
		$internship_id=(int)$internship_id;
		$query='UPDATE internships
				SET internship_state=\'exporte\'
				WHERE internship_id='.$internship_id;
		$this->_db->prepare($query)->execute();
	}
	public function superviseInternship($internship_id,$new_supervisor_email){
		$internship_id=(int)$internship_id;
		$query='UPDATE internships
				SET internship_state=\'supervise\', supervisor_email='.$this->_db->quote($new_supervisor_email).'
				WHERE internship_id='.$internship_id;
	
		$this->_db->prepare($query)->execute();
	}


	private static function convertInternshipStateForDislay($internship_state){
		if($internship_state=="soumis") return "Soumis";
		if($internship_state=="soumisEtPris") return "Soumis et pris";
		if($internship_state=="libre") return "Libre";
		if($internship_state=="complete") return "Complété";
		if($internship_state=="attribue") return "Attribué";
		if($internship_state=="exporte") return "Exporté";
		if($internship_state=="supervise") return "Supervisé";
		if($internship_state=='noMatchingInternship') return 'Pas de stage';
	}
	
	public function checkFirstLastNameStudent($registration_id) {
		$registration_id=htmlentities($registration_id);
		$query='SELECT DISTINCT first_name, last_name, email
				FROM students
				WHERE registration_id='.$this->_db->quote($registration_id);
		$result=$this->_db->query($query);
		$row=$result->fetch();
		$stud=array();
		$stud['first_name']=$row->first_name;
		$stud['last_name']=$row->last_name;
		$stud['email']=$row->email;
		return $stud;
	}

	
	/////////INSERTION DB FORMULAIRE : ////////
	public function setOrUpdateInternship($details){
		if(empty($details['internship_id'])){
			$company_id=$this->setNewCompany($details);
			$registration_id=$this->getRegistrationId($details['first_name_student'],$details['last_name_student']);
			$person_to_contact=$this->setNewContact($details);
			$promoter_id=$this->setNewPromoter($details);
			if($details['first_name_student']!="/" and $details['first_name_student']!="/"){
				$registration_id=$this->getRegistrationId($details['first_name_student'],$details['last_name_student']);
				$this->updateStudentPhone($registration_id,$details['student_phone']);
			}
			$this->setNewInternshipBasicInfo($details,$registration_id,$company_id, $person_to_contact,$promoter_id);
		}
		else{

			$internshipToUpdate=(int)$details['internship_id'];
			$this->updateCompany($details,$internshipToUpdate);
			$this->updateContact($details,$internshipToUpdate);
			if($this->noPromoter($internshipToUpdate)){
				$promoter_id=$this->setNewPromoter($details);
				$this->setPromoter_id($internshipToUpdate,$promoter_id);
			}
			else $this->updatePromoter($details,$internshipToUpdate);
			if($details['first_name_student']!="/" and $details['first_name_student']!="/"){
				$registration_id=$this->getRegistrationId($details['first_name_student'],$details['last_name_student']);
				$this->updateStudentPhone($registration_id,$details['student_phone']);
			}
		
			$this->updateInternshipBasicInfo($details,$internshipToUpdate);
		}
	}
	public function noPromoter($internshipToUpdate){
		$query='SELECT COUNT(internship_id)
				FROM internships 
				WHERE internship_id='.$internshipToUpdate.'
						AND promoter_id IS NULL';
		$result=$this->_db->query($query);
		if(!$result && count($result)==0) return false;
		else return true;
	}
	public function setPromoter_id($internshipToUpdate,$promoter_id){
		$query='UPDATE internships
				SET promoter_id='.$this->_db->quote($promoter_id).'
				WHERE internship_id='.$internshipToUpdate;
		$this->_db->prepare($query)->execute();
	}

	public function setNewCompany($details){
		$query='INSERT INTO companies (name, address,zip,locality,secretaryship_phone)
				VALUES ('.$this->_db->quote($details['company_name']).','.$this->_db->quote($details['company_address']).','.$this->_db->quote($details['company_zip']).','.$this->_db->quote($details['company_locality']).',
					 '.$this->_db->quote($details['secretaryship_phone']).')';
		$this->_db->prepare($query)->execute();
		return $this->getCompany_id($details['company_name'],$details['company_address']);

	}
	public function getCompany_id($name, $address){
		$query='SELECT company_id
				FROM companies
				WHERE name='.$this->_db->quote($name).'
						AND address='.$this->_db->quote($address);
		
		$result = $this->_db->query($query);
		if($result->rowcount () != 0){
 			$return= $result->fetch()->company_id;
		}else{
 			$return=NULL;
 		}
 		return $return;
	}
	public function setNewContact($details){
		$query='INSERT INTO intern_contacts (first_name, last_name,email,phone_number,service, function)
				VALUES ('.$this->_db->quote($details['ic_first_name']).','.$this->_db->quote($details['ic_last_name']).','.$this->_db->quote($details['ic_email']).','.$this->_db->quote($details['ic_phone_number']).','.$this->_db->quote($details['ic_service']).','.$this->_db->quote($details['ic_function']).')';
		$this->_db->prepare($query)->execute();
		return $this->getContact_id($details['ic_first_name'],$details['ic_last_name']);
		
	}
	public function getContact_id($first_name, $last_name){
		$query='SELECT intern_contact_id
				FROM intern_contacts
				WHERE first_name='.$this->_db->quote($first_name).'
						AND last_name='.$this->_db->quote($last_name);
		$result=$this->_db->query($query);
		$intern_contact_id=$result->fetch()->intern_contact_id;
		return $intern_contact_id;
	}
	public function setNewPromoter($details){
		$query='INSERT INTO intern_contacts (first_name, last_name,email,phone_number)
				VALUES ('.$this->_db->quote($details['promoter_first_name']).','.$this->_db->quote($details['promoter_last_name']).','.$this->_db->quote($details['promoter_email']).','.$this->_db->quote($details['promoter_phone']).')';

		$this->_db->prepare($query)->execute();
		return $this->getContact_id($details['promoter_first_name'],$details['promoter_last_name']);
		
	}
	public function setNewInternshipBasicInfo($details,$registration_id, $company_id, $person_to_contact,$promoter_id){
		$query='INSERT INTO internships (registration_id,company_id,internship_state, person_to_contact,promoter_id,objectives, description, remarks, work_environment, intership_origin)
				VALUES ('.$this->_db->quote($registration_id).','.$this->_db->quote($company_id).','.$this->_db->quote($details['internship_state']).','.$this->_db->quote($person_to_contact).','.$this->_db->quote($promoter_id).','.$this->_db->quote($details['objectives']).','.$this->_db->quote($details['description']).','.$this->_db->quote($details['remarks']).','.$this->_db->quote($details['work_environment']).','.$this->_db->quote($details['internship_origin']).')';
		$this->_db->prepare($query)->execute();
		
	}
	public function updateCompany($details,$internshipToUpdate){
		$query='UPDATE companies CO
				INNER JOIN internships IT ON IT.company_id=CO.company_id
				SET CO.name='.$this->_db->quote($details['company_name']).', CO.address='.$this->_db->quote($details['company_address']).'
					 ,CO.zip='.$this->_db->quote($details['company_zip']).', CO.locality='.$this->_db->quote($details['company_locality']).',
					 CO.secretaryship_phone='.$this->_db->quote($details['secretaryship_phone']).'
				WHERE IT.internship_id='.$internshipToUpdate;
		$this->_db->prepare($query)->execute();

	}
	public function updateCompanyInfo($company_id,$company_name,$company_address,$company_phone,$company_zip,$company_locality){
		$query='UPDATE companies
				SET name='.$this->_db->quote($company_name).',
					address='.$this->_db->quote($company_address).',
					secretaryship_phone='.$this->_db->quote($company_phone).',
					zip='.$this->_db->quote($company_zip).',
					locality='.$this->_db->quote($company_locality).'
				WHERE company_id='.$company_id;
		$this->_db->prepare($query)->execute();
	}
	public function updateContactInfo($intern_contact_id,$contact_first_name,$contact_last_name,$contact_email,$contact_phone,$contact_service,$contact_function){
		$query='UPDATE intern_contacts
				SET first_name='.$this->_db->quote($contact_first_name).',
					last_name='.$this->_db->quote($contact_last_name).',
					email='.$this->_db->quote($contact_email).',
					phone_number='.$this->_db->quote($contact_phone).',
					service='.$this->_db->quote($contact_service).',
					function='.$this->_db->quote($contact_function).'
				WHERE intern_contact_id='.$intern_contact_id;
		$this->_db->prepare($query)->execute();
	}
	public function updateContact($details,$internshipToUpdate){
		$query='UPDATE intern_contacts IC
				INNER JOIN internships IT ON IT.person_to_contact=IC.intern_contact_id
				SET IC.first_name='.$this->_db->quote($details['ic_first_name']).', IC.last_name='.$this->_db->quote($details['ic_last_name']).'
					 ,IC.email='.$this->_db->quote($details['ic_email']).', IC.phone_number='.$this->_db->quote($details['ic_phone_number']).'
					 ,IC.service='.$this->_db->quote($details['ic_service']).'
					 ,IC.function='.$this->_db->quote($details['ic_function']).'
				WHERE IT.internship_id='.$internshipToUpdate;
		$this->_db->prepare($query)->execute();
		
	}

	public function updatePromoter($details,$internshipToUpdate){
		$query='UPDATE intern_contacts IC
				SET IC.first_name='.$this->_db->quote($details['promoter_first_name']).', IC.last_name='.$this->_db->quote($details['promoter_last_name']).'
					  , IC.email='.$this->_db->quote($details['promoter_email']).', IC.phone_number='.$this->_db->quote($details['promoter_phone']).', IC.service='.$this->_db->quote($details['promoter_service']).'
				WHERE IC.intern_contact_id IN (SELECT IT.promoter_id FROM internships IT WHERE IT.internship_id='.$internshipToUpdate;
		$this->_db->prepare($query)->execute();
		
	}
	public function updateStudentPhone($registration_id, $phone_number){
		$phone_number=htmlentities($phone_number);
		$query='UPDATE students
				SET phone_number='.$this->_db->quote($phone_number).'
				WHERE registration_id='.$registration_id;
		$this->_db->prepare($query)->execute();
	}
	public function updateInternshipBasicInfo($details,$internshipToUpdate){
		if($details['internship_state']=='complete'){
			$query='UPDATE internships
					SET internship_state='.$this->_db->quote($details['internship_state']).',objectives='.$this->_db->quote($details['objectives']).
					', description='.$this->_db->quote($details['description']).', remarks='.$this->_db->quote($details['remarks']).'
					, work_environment='.$this->_db->quote($details['work_environment']).', intership_origin='.$this->_db->quote($details['internship_origin']).'
					WHERE internship_id='.$internshipToUpdate;
		}
		elseif($details['internship_state']=='attribue'){
			$query='UPDATE internships
					SET internship_state='.$this->_db->quote($details['internship_state']).',objectives='.$this->_db->quote($details['objectives']).
					', description='.$this->_db->quote($details['description']).', remarks='.$this->_db->quote($details['remarks']).'
					, work_environment='.$this->_db->quote($details['work_environment']).', intership_origin='.$this->_db->quote($details['internship_origin']).',
						registration_id='.$this->_db->quote($details['registration_id']).'
					WHERE internship_id='.$internshipToUpdate;
		}
		else{
			$query='UPDATE internships
					SET objectives='.$this->_db->quote($details['objectives']).
					', description='.$this->_db->quote($details['description']).', remarks='.$this->_db->quote($details['remarks']).'
					, work_environment='.$this->_db->quote($details['work_environment']).', intership_origin='.$this->_db->quote($details['internship_origin']).'
					WHERE internship_id='.$internshipToUpdate;
		}

		$this->_db->prepare($query)->execute();		
	}
	public function updateStudentInfo($registration_id,$student_first_name,$student_last_name,$student_email,$student_phone){
		$query='UPDATE students
				SET first_name='.$this->_db->quote($student_first_name).',
					last_name='.$this->_db->quote($student_last_name).',
					email='.$this->_db->quote($student_email).',
					phone_number='.$this->_db->quote($student_phone).'
				WHERE registration_id='.$this->_db->quote($registration_id);
		$this->_db->prepare($query)->execute();
	}
	public function updateTeacherFunction($email_unique_id,$is_admin){
		$query='UPDATE teachers
				SET is_admin='.$is_admin.' 
				WHERE email_unique_id='.$this->_db->quote($email_unique_id);
		$this->_db->prepare($query)->execute();
	}

//////////////////INSERTION DB FORMULAIRE ENTREPRISE///////////////////
	
	public function insert_contact($first_name, $last_name, $phone_number, $email) {
		$query = $this->_db->prepare ( 'INSERT INTO intern_contacts (first_name, last_name, phone_number, email) VALUES (:first_name,:last_name,:phone_number,:email)' );
 		
		$query->execute ( [ 
				'first_name' => $first_name,
				'last_name' => $last_name,
				'phone_number' => $phone_number,
				'email' => $email 
		] );
	}
	public function insert_company($name, $address, $locality,$zip,$secretaryship_phone) {
		$stmt = $this->_db->prepare ( 'INSERT INTO companies (name, address,locality,zip,secretaryship_phone) VALUES (:name,:address,:locality,:zip,:secretaryship_phone)' );
		$stmt->execute ( [ 
				'name' => $name,
				'address' => $address,
				'locality' => $locality,
				'zip'=>$zip,
				'secretaryship_phone' => $secretaryship_phone
		] );
 	}
 	public function insert_internship($person_to_contact,$company_id, $objectives, $remarks, $work_environment, $description) {
		$query = $this->_db->prepare ( 'INSERT INTO internships (person_to_contact,company_id, objectives, remarks, work_environment,description,internship_state) VALUES (:person_to_contact,:company_id,:objectives,:remarks,:work_environment,:description,\'soumis\')' );
 		
		$query->execute ( [ 
				'person_to_contact' => $person_to_contact,
				'company_id' => $company_id,
				'objectives' => $objectives,
				'remarks' => $remarks,
				'work_environment' => $work_environment,
				'description' => $description 
		] );
 	}
 	////////// FIN INSERTION DB FORMULAIRE///////////////
	
	
	public function select_company() {
		$query = 'SELECT DISTINCT CO.*
		        FROM companies CO, internships IT
		        WHERE IT.company_id=CO.company_id
		        	 AND IT.internship_state=\'libre\'';
		$result = $this->_db->query ( $query );
		$row = $result->fetch ();
		$tableau = array ();
		if ($result->rowcount () != 0) {
			while ( $row = $result->fetch () ) {
				$tableau [] = new Company ( $row->company_id, $row->name, $row->address, $row->zip, $row->locality, $row->secretaryship_phone );
			}
 		}
 		return $tableau;
	}
	
	
	////////// FIN INSERTION DB FORMULAIRE///////////////

	///////////SELECT POUR LES PROPOSITIONS DE STAGES//////////////
	public function select_company_table() {
		$query = 'SELECT DISTINCT CO.name , CO.locality, IT.objectives, IT.internship_id
				FROM companies CO, internships IT
				WHERE IT.company_id=CO.company_id
				AND IT.internship_state=\'libre\'';
		$result = $this->_db->query ( $query );
		$internships = array ();
		if ($result->rowcount () != 0) {
			while ( $row = $result->fetch () ) {
				$internships [$row->internship_id] ['company_name'] = $row->name;
				$internships [$row->internship_id] ['objectives'] = $row->objectives;
				$internships [$row->internship_id] ['locality'] = $row->locality;
				$internships [$row->internship_id] ['internship_id'] = $row->internship_id;
			}
		}
		return $internships;
 	}
 	//////////FIN DU SELECT POUR LES PROPOSITIONS DE STAGES/////////////
	
 	public function get_internship_state($internship_id){
 		$query = 'SELECT internship_state
 				FROM internships
 				WHERE internship_id =' . $internship_id;
 		$result = $this->_db->query ($query);
 		$internships ;
 		if($result->rowcount () != 0){
 			$internshipState= $result->fetch()->internship_state;
 		}else{
 			$internshipState=NULL;
 		}
 		return $internshipState;
 	}
 	
}

<section>
<?php if($error) echo "
		<div class=\"alert alert-danger\">
	  		<strong>Danger! </strong>" . $messError ."
		 </div>";
	?>
	<?php if($success) echo "
		<div class=\"alert alert-success\">
	  		<strong>OK ! </strong>" . $messSuccess ."
		 </div>";
	?>
	<div class="row">
		<div class="col-md-10">
			<form action="index.php?action=supervisor" method="POST">
				<div class="panel panel-primary filterable">
			        <div class="panel-heading">
			            <h3 class="panel-title">Liste stages</h3>
			            <div class="pull-right">
				           <button type="submit"  class="btn btn-default "><span class="glyphicon glyphicon-filter"></span> Activer Filtres</button>
				      	</div>
			        </div>
			        <table class="table">
			            <thead>
			                <tr class="filters">
			                		
				                    <th><input type="text" class="form-control" name="first_name" placeholder="Prénom"></th>
				                    <th><input type="text" class="form-control" name="last_name" placeholder="Nom"></th>
				                    <th><input type="text" class="form-control" name="company_name" placeholder="Société"></th>
				           			<th>Premier Validateur</th>
				           			<th>Deuxième Validateur</th>
				           			<th>Promoteur</th>
				           			<th>Contact interne</th>
									<th><button type="submit" name="see_details"  class="btn btn-primary">Détails </button></th>>
									<th> <button type="submit" name="exportStudents" value='ok'  class="btn btn-success"> Exporter </br> stage  PDF </button></th>
			                </tr>
			            </thead>
			            <tbody>
			            <?php foreach($internships as $key=>$internship){?>
			             	<tr>
								<td><?php echo $internship['first_name_student'] ;?></td>
								<td><?php echo $internship['last_name_student'] ;?></td>
								<td><?php echo $internship['company_name'] ;?></td>
								<td><?php echo $internship['first_validator']?></td>
								<td><?php echo $internship['second_validator']?></td>
								<td><?php echo $internship['promoter']?></td>
								<td><?php echo $internship['intern_contact']?></td>
								<td class="bs-radio"> 
				             		<input type="radio" name="see_details" value=<?php echo $key; ?> />
				             	</td>
				             	<td class="bs-radio"> 
				             		<input type="radio" name="exportateInternships" value=<?php echo $key; ?>  <?php if($internship['internship_state']!="Supervisé") echo "disabled"; ?> > 
				             	</td>											
							</tr>
						<?php } ?>
			            </tbody>
			        </table>
				</div>
			</form>	
		</div>
		<div class="col-md-2">
			<div id="RightSideBar">
				<div id="UserProfilBox">
					<h3 class="blockHeader">
						<span class="userName"><?php echo htmlentities($_SESSION['first_name'])." ".htmlentities($_SESSION['last_name']);   ?></span>
					</h3>
					<div id="userProfile">
						<div id="userPicture">
							<img class="userPicture" src="views/pictures/profilPicture.png" alt="Image de l'utilisateur">
						</div>
						<div id="userDetails">
							<p>
								<span class="bold">E-Mail :</span>
								<br><?php echo htmlentities($_SESSION['email']) ?>
							</p>
							<p> 
								<span class="bold">Fonction :</span>
								Superviseur
							</p>
							<p>
								<span class="bold">Date début projet : </span> <?php echo $config['dateBegin'] ?>
							</p>

							<p>
								<span class="bold">Date Fin projet : </span> <?php echo $config['dateEnd'] ?>
							</p>
							<p>
								<span class="bold">Date Butoire projet :</span> <?php echo $config['dateVeryEnd']?>
							</p>
						
							<p>
							<?php if(!$change_password) echo "
								<a href=\"index.php?action=supervisor&change_password=ok\">
									<img class=\"modif\" src=\"views/pictures/iconModif.png\" alt=\"Changer mon mot de passe personnel\">
										Changer mon mot de passe personnel
								</a>"; ?>
								<form action="index.php?action=supervisor&change_password=ok" method="POST">
								<?php if($change_password)	echo"
												<input class=\"form-control\" type=\"text\"  name=\"current_password\"  placeholder= \"Entrez votre mot de passe actuel\"/> 
												<input class=\"form-control\" type=\"password\"  name=\"first_password\"  placeholder=\"Entrez votre nouveau mot de passe\"/>
												<input class=\"form-control\" type=\"password\"  name=\"second_password\"  placeholder=\"Confirmer votre mot de passe\"/>
												<a class=\"btn btn-primary\" href=\"index.php?action=supervisor\"> Annuler </a>
												<input type=\"submit\" value=\"Changer mot de passe\" class=\"btn btn-warning\"/>";
								 ?> 
								 </form>
							</p>			
							<p>
								<?php if(!empty($_SESSION['adminAuthentification'])){
									echo "<a href=\"index.php?action=admin\" class=\"btn btn-link\">Gestion stages</a>";

								}?>
						
								<a href="index.php?action=logout" class="btn btn-danger">Déconnexion</a>
							</p>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<h1> CONVENTION DE STAGE </h1>
<!-- définition d'une variable nom selon mon propre langage -->

<p> Entre les soussignés </p>
<ul>
	<li> l'Institut Paul Lambin, Clos Chapelle-aux-Champs 43, 1200 Bruxelles, ici représenté par
Monsieur Benoît Dupuis, Directeur, ci-après nommé "l'Institut", </li>
	<li>#student_first_name# #student_last_name# ,étudiant en 3° année Baccalauréat en Informatique, ci-après nommé "l'Etudiant", 
		</li>
	<li>la société ,#company_name#, représentée par #promoter_first_name# #promoter_last_name#, ci-après nommée "l'Entreprise", 
	</li>
</ul>


<p> il a été convenu ce qui suit :  </p>
<h2> Art.1 </h2>

<p> L'Entreprise accepte de prendre en stage “l'Etudiant” et désigne un membre de son
personnel comme maître de stage. 
</p>


<h2> Art.2 </h2>

<p> Le stage débute le 1er février 2016 et prend fin le 21 mai 2016 (moyennant accord des
différentes parties, cette période pourrait être prolongée).
Les prestations s'effectuent 5 jours par semaine aux heures prévues par l'Entreprise. Le calendrier
des congés scolaires n’est pas d’application. L’Etudiant doit effectuer ses prestations dans les locaux
de l’Entreprise. </p>


<h2> Art.3 </h2>
<p> L'Entreprise informera Madame Colette De Muylder ou Monsieur Bernard Henriet,
responsables des stages au sein de l’Institut de toute absence de “l'Etudiant” durant les jours prévus
pour l'exécution du stage. 
</p>
<h2> Art.4 </h2>
<p> Il n'existe pas de convention de travail entre l'Etudiant et l'Entreprise, celui-ci continue à
relever de l'Institut auquel il appartient. Les conséquences en sont que: 
</p>
<ol>
	<li>Le stagiaire n'est pas soumis à la législation relative à la sécurité sociale. Aucune
cotisation ne peut donc être mise à charge de l'Entreprise. </li>
	<li> La législation concernant les accidents de travail et les accidents survenus sur le
chemin à l'aller et au retour du travail, de même celle concernant les maladies
professionnelles ne sont pas d'application pour l'Etudiant. </li>
</ol>
<p>L'établissement d'enseignement est tenu de prendre toutes les dispositions pour couvrir les risques
d'accidents et de maladie sans qu'il en résulte la moindre charge pour l'Entreprise. 
L'Institut prend l'engagement de renoncer à toute action en dommages, soit personnelle, soit par
l'intermédiaire d'un assureur, pour les accidents dont l'étudiant aura pu être victime.  </p>
<br>   <br>    <br> 

<h2> Art.5 </h2>
<p> L'étudiant ne peut recevoir de rémunérations. 
</p>
<h2> Art.6 </h2>
<p> L'Entreprise s'engage à permettre au stagiaire d'élaborer, à partir du travail réalisé, un
rapport écrit qui sera défendu par l'étudiant devant un jury; un membre de l'Entreprise sera invité à
assister à cette défense. 
</p>
<p>Avant toute officialisation du rapport, l'étudiant stagiaire le soumet au maître de stage et se
conforme aux recommandations de ce dernier.
L'étudiant stagiaire s'engage à ne pas divulguer, même après le stage, les informations
confidentielles de l'entreprise auxquelles il aurait pu avoir accès.  </p>
<p>Les trois parties reconnaissent que tous les droits intellectuels sur les programmes créés pendant le
stage, ainsi que le droit de commercialisation de ces programmes, appartiennent entièrement et
uniquement à l'Entreprise. 
 </p>
<h2> Art.7 </h2>
<p> L'Institut désignera un professeur qui se chargera durant le stage de prendre contact avec
l'entreprise afin de suivre l'évolution du travail réalisé. </p>

<p>Fait en trois exemplaires, à Bruxelles, le #date#  </p>

<div class="bottomPage1" > L'Entreprise
</div>
<div class="bottomPage1" > L'Etudiant
</div>
<div class="bottomPage1" > L'institut
</div>





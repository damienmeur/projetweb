
<img src="views/pictures/logo_vinci.png" alt="logo vinci"/>

<h1> Renseigements concernants le stage </h1>
<h2> ETUDIANT-STAGIAIRE :</h2>
<table>
	<tr>
		<th>Nom : </th>
		<th>Prénom : </th>
		<th>Adresse électronique : </th>
		<th>Numéro de gsm : </th>
	</tr>
	<tr>
		<td>#student_last_name# </td>
		<td>#student_first_name# </td>
		<td>#student_email#</td>
		<td>#student_phone#</td>
	</tr>

</table>
<h2> SOCIETE DU STAGE :</h2>
<table>
	<tr>
		<th>Dénomination complète : </th>
		<th>Adresse : </th>
		<th>Localité : </th>
		<th>Code postal : </th>
	</tr>
	<tr>
		<td>#company_name# </td>
		<td>#company_address#</td>
		<td>#company_locality#</td>
		<td>#company_zip#</td>
	</tr>

</table>

<h2> RESPONSABLE ADMINISTRATIF DU STAGE (c.-à.-d. la personne qui <span class="bold"> signe la convention</span>) :</h2>
<table>
	<tr>
		<th>Nom : </th>
		<th>Prénom : </th>
		<th>Fonction : </th>
		<th>email : </th>
		<th>GSM : </th>
	</tr>
	<tr>
		<td>#ic_last_name#</td>
		<td>#ic_first_name# </td>
		<td>#ic_service#</td>
		<td> #ic_email#</td>
		<td>#ic_phone_number# </td>
	</tr>

</table>

<h2> PROMOTEUR DU STAGE (c.-à.-d. la personne qui <span class="bold"> encadre</span>) l'étudiant dans la société :</h2>
<table>
	<tr>
		<th>Nom : </th>
		<th>Prénom : </th>
		<th>Service : </th>
		<th>email : </th>

	</tr>
	<tr>
		<td>#promoter_last_name#  </td>
		<td>#promoter_first_name# </td>
		<td>#promoter_service# </td>
		<td> #promoter_email# </td>

	</tr>
	
</table>
<h2> SUJET DE L'APPLICATION PROPOSEE (à expliciter en quelques lignes)</h2>
<p>"#description#</p>

<h2> Environnement de travail : logiciciels, langages, technologies :</h2>
<p>"#work_environment#</p>

<div class="bottomBox">
	<p class="underlined"> Lu et et approuvé </p> </br></br></br>
	<p class="underlined"> Signature du Promoteur de stage </p> </br></br></br>


</div>
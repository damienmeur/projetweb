<section>
	<?php if($error) echo "
		<div class=\"alert alert-danger\">
	  		<strong>Danger! </strong>" . $messError ."
		 </div>";
	?>
	<?php if($success) echo "
		<div class=\"alert alert-success\">
	  		<strong>OK ! </strong>" . $messSuccess ."
		 </div>";
	?>
<div class="row">
		<div class="col-md-10"> </div>
		<div class="col-md-2">
			<a class="btn btn-success btn-md login centerer" href="index.php?action=admin&view_choice=teachers_list_admin.php&exportTeachers=ok">Exporter les professeurs en csv </a>
		</div>
	</div>
<div class="row">
	<div class="col-md-10">
		<form action="index.php?action=admin&view_choice=teachers_list_admin.php" method="POST">
			<div class="btn-group btn-group-justified">
			  <a href="index.php?action=admin&view_choice=internships_list_admin.php" class="btn btn-primary">Stages</a>
			  <a href="index.php?action=admin&view_choice=students_list_admin.php" class="btn btn-primary">Etudiants</a>
			  <a href="#" class="btn btn-primary disabled">Professeurs</a>
			  <a href="index.php?action=admin&view_choice=contacts_list_admin.php" class="btn btn-primary">Contacts/promoteurs</a>
			  <a href="index.php?action=admin&view_choice=companies_list_admin.php" class="btn btn-primary">Sociétés</a>
			</div>
			<div class="panel panel-primary filterable">
		        <div class="panel-heading">
		            <h3 class="panel-title">Liste professeurs</h3>
		           <div class="pull-right">
				        <button type="submit"  class="btn btn-default "><span class="glyphicon glyphicon-filter"></span> Activer Filtres</button>
				     </div>
		        </div>
		        <table class="table">
		            <thead>
		                <tr class="filters">
		                	<th>Nombre de stages</th>
		                    <th><input type="text" class="form-control" name="first_name" placeholder="Prénom"></th>
		                    <th><input type="text" class="form-control" name="last_name" placeholder="Nom"></th>
		                    <th>E-mail</th>
		                    <th>Fonction</th>
		                    <th><button type="submit" name="modify_teacher_info" value="ok" class="btn btn-warning">Modifier info professeur</button></th>
		                     <th><button type="submit" name="delete_teacher_info"  value="ok" class="btn btn-warning">Désactiver professeur</button></th>
		 

		                </tr>
		            </thead>
		            <tbody>
		            	<?php if(empty($teachers)) echo "Aucun résultat avec les filtres appliqués." ?>
		            	<?php foreach($teachers as $key => $teacher){ ?>
		             	<tr>
		             		<?php  if($email_unique_id_to_modify==$key) echo "<input type=\"hidden\" name=\"email_unique_id_to_modify\" value=".$key." />"?>
		             		<?php  if($email_unique_id_to_delete==$key) echo "<input type=\"hidden\" name=\"email_unique_id_to_delete\" value=".$key." />"?>
		           
							<td><?php echo  $teacher['internship_state']; ?></td>
							<td><?php  echo $teacher['first_name']?></td>
							<td><?php  echo $teacher['last_name']?></td>
							<td><?php echo $key?></td>
							<td><?php if($email_unique_id_to_modify==$key){
											 echo"<select class=\"form-control\"  name=\"teacher_function\" value=".$teacher['function']."/>";
											 if($teacher['function']=="Superviseur"){
											 	echo "<option name=\"teacher_function\" value=\"Superviseur\" selected > Superviseur </option>";
											 	echo "<option name=\"teacher_function\" value=\"Responsable de stage\"  > Responsable de stage </option>";
											 }
											 else{
											 	echo "<option name=\"teacher_function\" value=\"Responsable de stage\" selected > Responsable de stage </option>";
											 	echo "<option name=\"teacher_function\" value=\"Superviseur\"  > Superviseur </option>";
											 }
											 echo "</select>" ;	
										}
										else echo $teacher['function']
								?>
							</td>
				
						 	<?php if($email_unique_id_to_modify!=$key) echo "
									<td class=\"bs-radio\">
					             		<input type=\"radio\" name=\"modify_teacher_details\" value=".$key."  />
					             	</td>";
					             	 	else echo "
					             	 		<td>
					             	 			<input class=\"btn btn-danger\" name=\"confirm_modify_teacher\" value=\"ok\" type=\"submit\" value=\"Confirmer\" />
					             	 		</td>";?>
					         <?php if($email_unique_id_to_delete!=$key) echo "
									<td class=\"bs-radio\">
					             		<input type=\"radio\" name=\"set_unactive\" value=".$key."  />
					             	</td>";
					             	 	else echo "
					             	 		<td>
					             	 			<input class=\"btn btn-danger\" name=\"comfirm_delete_teacher\" value=\"ok\"type=\"submit\" value=\"Confirmer\" />
					             	 		</td>";?>
						</tr>
						<?php } ?>
		            </tbody>
		        </table>
			</div>
		</form>
	</div>
	<div class="col-md-2">
			<div id="RightSideBar">
				<div id="UserProfilBox">
					<h3 class="blockHeader">
						<span class="userName"><?php echo htmlentities($_SESSION['first_name'])." ".htmlentities($_SESSION['last_name']);   ?></span>
					</h3>
					<div id="userProfile">
						<div id="userPicture">
							<img class="userPicture" src="views/pictures/profilPicture.png" alt="Image de l'utilisateur">
						</div>
						<div id="userDetails">
							<p>
								<span class="bold">E-Mail :</span>
								<br><?php echo htmlentities($_SESSION['email']) ?>
							</p>
						
							<p> 
								<span class="bold">Fonction :</span>
								Responsable de stage
							</p>
							<form action="index.php?action=admin" method="POST">
								<p>
									<span class="bold">Date début projet : </span> <?php if(!$date_grid_mod) echo $config['dateBegin'] ; else echo "<input type=text class=\"form-control\" name=\"dateBegin\">"; ?>
								</p>

								<p>
									<span class="bold">Date début projet : </span> <?php if(!$date_grid_mod) echo $config['dateEnd'] ; else echo "<input type=text class=\"form-control\" name=\"dateEnd\">"; ?>
								</p>
								<p>
									<span class="bold">Date début projet : </span> <?php if(!$date_grid_mod) echo $config['dateVeryEnd'] ; else echo "<input type=text class=\"form-control\" name=\"dateVeryEnd\">"; ?>
								</p>
								<p>
									<?php if(!$date_grid_mod) echo"<input type=\"submit\" name=\"date_grid_mod\" value=\"Modifier dates\" class=\"btn btn-warning\"/>";
										  else  echo"<input type=\"submit\" name=\"date_grid_mod\" value=\"Confirmer modifications\" class=\"btn btn-success\"/>"?>
				           
								</p>
							</form>
							<p>
								
								<form enctype="multipart/form-data" action="index.php?action=admin&view_choice=internships_list_admin.php" method="post">
									<label class="control-label">Uploader fichier étudiants :</label>
		     						<input type="hidden" name="MAX_FILE_SIZE" value="1000000000000" />
									<input type="file" name="userfile" class="btn"/>
									<input type="submit" class="btn btn-success"/>
								</form>
								<?php if(!empty($_FILES['userfile']))
									echo $messUpload;
								?>	
							</p>
					

							<p>
								<?php if(!$change_password) echo "
								<a href=\"index.php?action=admin&change_password=ok&view_choice=teachers_list_admin.php\">
									<img class=\"modif\" src=\"views/pictures/iconModif.png\" alt=\"Changer mon mot de passe personnel\">
										Changer mon mot de passe personnel
								</a>"; ?>
								<form action="index.php?action=admin&change_password=ok" method="POST">
								<?php if($change_password)	echo"
												<input class=\"form-control\" type=\"text\"  name=\"current_password\"  placeholder= \"Entrez votre mot de passe actuel\"/> 
												<input class=\"form-control\" type=\"password\"  name=\"first_password\"  placeholder=\"Entrez votre nouveau mot de passe\"/>
												<input class=\"form-control\" type=\"password\"  name=\"second_password\"  placeholder=\"Confirmer votre mot de passe\"/>
												<a class=\"btn btn-primary\" href=\"index.php?action=admin&view_choice=teachers_list_admin.php\"> Annuler </a>
												<input type=\"submit\" value=\"Changer mot de passe\" class=\"btn btn-warning\"/>";
								 ?> 
								 </form>
							</p>
							<p>
								<a href="index.php?action=supervisor" class="btn btn-link">Voir stages </br> personnels</a>	
								<a href="index.php?action=logout" class="btn btn-danger">Déconnexion</a>
							</p>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

	

</section>
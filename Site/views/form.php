<section id="formPage">
	<div class="row">
		<div class="col-sm-4">
			<div class="list-group" style="height:400px; overflow-y:scroll">
				<a class="list-group-item">
					
					<h4 class="list-group-item-text">Avez-vous trouvé un stage via la liste de propositions? Sélectionnez le ci-dessous pour pré-remplir le formulaire</h4>
				</a> 
			<form method="POST" action = "index.php?action=form&mode=grid_mode_student">	<?php foreach ($internships as $internship_id => $internship) {?>
					<a class="list-group-item">
					<input type="radio" value=<?php echo $internship['internship_id']?> name="id_stage">
					<h4 class="list-group-item-heading"><?php echo $internship['company_name']?></h4>
					<p class="list-group-item-text"><?php echo $internship['objectives']?></p>
					<p class="list-group-item-text"><?php echo $internship['locality']?></p>
				</a> 
				<?php }?>
				<input id="submit_internship" type="submit" name="submit_internship" value="Sélectionner">
				</form> 
			</div>
		</div>
		<fieldset style=" height:400px; overflow-y:scroll">
	
			<form class="form-inline" method="POST" <?php if($admin and !$complete_mode) echo "action=index.php?action=form&mode=grid_mode_admin&validate=ok&internship_id=".$internship_to_see; elseif($admin and $complete_mode) echo "action=index.php?action=form&mode=complete_mode_promoter&validate=ok&internship_id=".$internship_to_see; elseif(!$admin and $isFirst) echo "action=index.php?action=form&mode=grid_mode_student&validate=ok" ; elseif($pre_encoded) echo "action=index.php?action=form&mode=pre_encoded_mode&validate=ok&internship_id=".$internship_to_see ;  ?>/>
				<h4 class="subtitle-form">Etudiant(e)</h4>
				<div class="form-group">
					<label for="exampleInputName2">Nom</label> <input disabled type="text"
						class="form-control" name="last_name_student" id="exampleInputName2"  <?php if($see_mode) echo "value=\"".$details['last_name_student']."\""; ?> >
				</div>
				<div class="form-group">
					<label for="exampleInputName2">Prénom</label> <input disabled type="text"
						class="form-control"  name="first_name_student" id="exampleInputName2" <?php if($see_mode) echo "value=\"".$details['first_name_student']."\""; ?> >
				</div>

				<div class="form-group">
					<label for="exampleInputEmail2">Mail</label> <input disabled type="text"
						class="form-control"  name="student_email" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=".$details['student_email']; ?> >
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">GSM</label> <input type="text"
						class="form-control" name="student_phone" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=\"".$details['student_phone']."\"";?>>
				</div>
				
				<h4 class="subtitle-form">Société Proposée</h4>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Dénomination</label> <input type="text"
						class="form-control" id="exampleInputEmail2" name="company_name"
					<?php if($see_mode ) echo "value=\"".$details['company_name']."\"";
					if(!$isFirst) echo " disabled"?>/>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Adresse</label> <input type="text"
						class="form-control" id="exampleInputEmail2" name="company_address"
						<?php if($see_mode) echo " value=\"". $details['company_address']."\"";
						if(!$isFirst) echo " disabled"?>/>

				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Localité</label> <input type="text"
						class="form-control" id="exampleInputEmail2"  name="company_locality"
						<?php if($see_mode ) echo "value=\"".$details['company_locality']."\"";
						if(!$isFirst) echo " disabled"?>>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2">Code postal</label> <input type="text"
						class="form-control" id="exampleInputEmail2"  name="company_zip"
						<?php if($see_mode ) echo "value=\"".$details['company_zip']."\"";
						if(!$isFirst) echo " disabled"?>>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2">Téléphone secrétariat</label> <input type="text"
						class="form-control" id="exampleInputEmail2"  name="secretaryship_phone"
						<?php if($see_mode ) echo "value=\"".$details['secretaryship_phone']."\"";
						if(!$isFirst) echo " disabled"?>>
				</div>
				
				
				
				<h4 class="subtitle-form">Personne de contact</h4>
				<div class="form-group">
					<label for="exampleInputName2">Nom</label> <input type="text" name="ic_last_name"
						class="form-control" id="exampleInputName2" 
						<?php if($see_mode) echo " value=\"".$details['ic_last_name']."\"";
								if(!$isFirst) echo " disabled" ?>>
				</div>
				<div class="form-group">
					<label for="exampleInputName2">Prénom</label> <input type="text" name="ic_first_name"
						class="form-control" id="exampleInputName2" <?php if($see_mode) echo " value=\"".$details['ic_first_name']."\"";
						if(!$isFirst) echo " disabled"?>>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Mail</label> <input type="text" name="ic_email"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=\"".$details['ic_email']."\"";
						if(!$isFirst) echo " disabled"?>>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Service</label> <input type="text" name="ic_service"
						class="form-control" id="exampleInputName2"
						<?php if($see_mode ) echo "value=\"".$details['ic_service']."\"";?>>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Téléphone</label> <input type="text" name="ic_phone_number"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=\"".$details['ic_phone_number']."\"";?>>

				</div>
				<div class="form-group">
					<label for="exampleInputEmail2">Fonction</label> <input type="text" name="ic_function"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=\"".$details['ic_function']."\"";?>>

				</div>
			
				<h4 class="subtitle-form">Informaticien disponible pour assurer l’encadrement “IT” du stage</h4>
				<div class="form-group">
					<label for="exampleInputName2">Nom</label> <input type="text" name="promoter_last_name"
						class="form-control" id="exampleInputName2" <?php if($see_mode) echo "value=\"".$details['promoter_last_name']."\"";?>>
				</div>
				<div class="form-group">
					<label for="exampleInputName2">Prénom</label> <input type="text" name="promoter_first_name"
						class="form-control" id="exampleInputName2" <?php if($see_mode ) echo "value=\"".$details['promoter_first_name']."\"";?>>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Service</label> <input type="text" name="promoter_service"
						class="form-control" id="exampleInputName2"
						<?php if($see_mode ) echo "value=\"".$details['promoter_service']."\"";?>>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Téléphone</label> <input type="text" name="promoter_phone"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode ) echo "value=\"".$details['promoter_phone']."\"";?>>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2">Mail</label> <input type="text" name="promoter_email"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode ) echo "value=\"".$details['promoter_email']."\"";?>>
				</div>
		
				
				<h5 class="subtitle-form">Description succincte du travail à effectuer :</h5>
					<textarea class="form-control" rows="3" cols="60" name="description">
					<?php if($see_mode ) echo $details['description'];
					?> </textarea>
				
				<h5 class="subtitle-form">Environnement de travail : logiciels, langages, technologies :</h5>
					<textarea class="form-control" rows="3" cols="60" name="work_environment">
					<?php if($see_mode ) {
						echo $details['work_environment'];
					}
					elseif($pre_encoded==true){
						echo $internship_form['work_environment'];
					}?></textarea>
					
				<h5 class="subtitle-form">D’ou vient la proposition (ecampus, relation, poursuite job, ...) ?</h5>
					<textarea class="form-control" rows="3" cols="60" name="internship_origin">
					<?php if($see_mode ) echo $details['internship_origin'];?>
					</textarea>
					<br>
			
					<?php if($grid_mode or $pre_encoded ) echo '<button type="submit" name="submit_form" class="btn btn-lg btn-success" value="submit">Envoyer</button';?>
					<?php if($complete_mode) echo '<button type="submit" class="btn btn-lg btn-success" value="submit">Marquer compléter</button';?>

			</form>
		</fieldset>
	
	</div>
	<div class="row">
		<div class="col-sm-4">
			<a href=<?php if($admin and !$complete_mode) echo "index.php?action=admin"; elseif($complete_mode and !empty($_SESSION['adminAuthentification']) and $_SESSION['adminAuthentification']==true) echo "index.php?action=admin&complete=success"; elseif($supervisor_mode) echo "index.php?action=supervisor&complete=success" ;  else echo "index.php?action=student"   ?>>
				<img src="views/pictures/goBackButton.png" alt="Revenir en arrière">
			</a>
		</div>
	</div>
</section>
<?php if(!empty ($_POST['submit']) && $error) echo "
		<div class=\"alert alert-danger\">
	  		<strong>Danger! </strong>" . $messError ."
		 </div>";
	?>
	<?php if($success) echo "
		<div class=\"alert alert-success\">
	  		<strong>OK ! </strong>" . $messSuccess ."
		 </div>";
	?>
<div class="container" style="margin-top: 10%">
	<div class="col-md-3 col-md-offset-4">
		<div class="panel member_signin">
			<div class="panel-body">
				<div class="fa_user">
					<i class="fa fa-user"></i>
				</div>
				<p class="member">Bienvenue</p>
				<form role="form" class="loginform" action="index.php?action=login" method="POST">
					<div class="form-group">
						<label for="exampleInputEmail1" class="sr-only">Email address</label>
						<div class="input-group">
							<input name="email_or_registration_number" type="text" class="form-control" id="exampleInputEmail1" 
								placeholder="E-mail/Matricule">
						</div>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1" class="sr-only" >Password</label>
						<div class="input-group">
							<input name="password" type="password" class="form-control"
								id="exampleInputPassword1" placeholder="Password">
						</div>
					</div>
					<button type="submit" class="btn btn-primary btn-md login">Connexion</button>
				</form>
				<p class="forgotpass">
					<a href="index.php?action=login&amp;see=form" class="small">Entreprise? Remplir le formulaire...</a>
					
				</p>
			</div>
		</div>
	</div>
</div>

<?php if(!empty($_GET['see']) && $_GET['see']=='form') {?>
	<?php $_GET=array()?>
	<fieldset style=" height:400px; overflow-y:scroll">
		<form class="form-inline" action="index.php?action=login&see=form" method="post">
			<?php echo $messError?>
			<h3 class="subtitle-form">Demande de stagiaire</h3>
			
			<div class="form-group">
				<label for="exampleInputEmail2">Nom de la société</label> <input type="text"
					class="form-control" id="exampleInputEmail2"
					placeholder="Apple" name = "entreprise_name">
			</div>
			
			<div class="form-group">
				<label for="exampleInputEmail2">Adresse</label> <input type="text"
					class="form-control" id="exampleInputEmail2"
					placeholder="Rue de Perk 23" name="entreprise_address">
			</div>
			<br>
			<div class="form-group">
				<label for="exampleInputEmail2">Code Postal</label> <input type="text"
					class="form-control" id="exampleInputEmail2"
					placeholder="1560" name="entreprise_zip">
			</div>
			
			<div class="form-group">
				<label for="exampleInputEmail2">Téléphone Secretariat</label> <input type="text"
					class="form-control" id="exampleInputEmail2"
					placeholder="0458.23.45.76" name="secretaryship_phone">
			</div>
			
			<div class="form-group">
					<label for="exampleInputEmail2">Localité</label> <input type="text"
						class="form-control" id="exampleInputEmail2" placeholder= "Bruxelles" name ="entreprise_locality">
			</div>
			
			<div class="subtitle-form">
				<label for="exampleInputEmail2">Nom de la personne que l’étudiant doit contacter:</label> <input type="text"
					class="form-control" id="exampleInputEmail2"
					placeholder="Jobs" name = "contact_name">
			</div>
			
			<div class="subtitle-form">
				<label for="exampleInputEmail2">Prénom de la personne que l’étudiant doit contacter:</label> <input type="text"
					class="form-control" id="exampleInputEmail2"
					placeholder="Jean" name = "contact_firstName">
			</div>
			
			<div class="form-group">
				<label for="exampleInputEmail2">Téléphone</label> <input type="text"
					class="form-control" id="exampleInputEmail2"
					placeholder="0478.23.45.12" name="contact_phone">
			</div>
			
			<div class="form-group">
				<label for="exampleInputEmail2">Mail</label> <input type="email"
					class="form-control" id="exampleInputEmail2"
					placeholder="loury.jacob@exemple.com" name="contact_mail">
			</div>
					
			
			
			<h5 class="subtitle-form">Objectifs du stage et description du travail à éffectuer :</h5>
			<textarea class="form-control" rows="3" cols="60" name="objectives"></textarea>
			
			<h5 class="subtitle-form">Brève description de l’environnement informatique du point de vue matériel et logiciel :</h5>
			<textarea class="form-control" rows="3" cols="60" name="work_environment"></textarea>
			
			<h5 class="subtitle-form">Brève description de l’environnement humain (travail en équipe, travail individuel, type de supervision prévue,...) :</h5>
			<textarea class="form-control" rows="3" cols="60" name="description"></textarea>
			

			<h5 class="subtitle-form">Autres remarques :</h5>
			<textarea class="form-control" rows="3" cols="60" name="remarks"></textarea>
			
			
			<p class="subtitle-form"> <a href="index.php?action=login"> Cacher le formulaire </a>
			<p class="subtitle-form"><input type="submit" name="submit" value="Envoyer" class="btn btn-lg btn-success"> </p>
		</form>
	</fieldset>

<?php }?>


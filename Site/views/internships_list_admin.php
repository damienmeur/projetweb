<section>
	<?php if($error) echo "
		<div class=\"alert alert-danger\">
	  		<strong>Danger! </strong>" . $messError ."
		 </div>";
	?>
	<?php if($success) echo "
		<div class=\"alert alert-success\">
	  		<strong>OK ! </strong>" . $messSuccess ."
		 </div>";
	?>
	<div class="row">
		<div class="col-md-10"> </div>
		<div class="col-md-2">
			<a class="btn btn-success btn-md login centerer" href="index.php?action=admin&exportInternships=ok">Exporter tous les stages exportés en pdf sur le serveur </a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10">
			<form <?php if(!$exportate) echo "action=\"index.php?action=admin\"" ; else echo "action=\"index.php?action=admin&exportate=ok";?>  method="POST">
				<div class="btn-group btn-group-justified">
				  <a href="#" class="btn btn-primary disabled">Stages</a>
				  <a href="index.php?action=admin&view_choice=students_list_admin.php" class="btn btn-primary">Etudiants</a>
				  <a href="index.php?action=admin&view_choice=teachers_list_admin.php" class="btn btn-primary">Professeurs</a>
				  <a href="index.php?action=admin&view_choice=contacts_list_admin.php" class="btn btn-primary">Contacts/promoteurs</a>
				  <a href="index.php?action=admin&view_choice=companies_list_admin.php" class="btn btn-primary">Sociétés</a>
				</div>
				<div class="panel panel-primary filterable">
			        <div class="panel-heading">
			            <h3 class="panel-title">Liste stages</h3>
			            <div class="pull-right">
			                <button type="submit"  class="btn btn-default "><span class="glyphicon glyphicon-filter"></span> Activer Filtres</button>
			            </div>
			        </div>
			        <table class="table">
			            <thead>
			                <tr class="filters">

			                	<th>
				           	     	<div class="dropdown">
				  						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Etat du stage<span class="caret"></span></button>
				  							<ul class="dropdown-menu">
				  								<li><a href="index.php?action=admin&internships_state_filter=tous&view_choice=internships_list_admin.php">Tous</a></li>
				  								<li><a href="index.php?action=admin&internships_state_filter=soumis&view_choice=internships_list_admin.php">Soumis</a></li>
				  							 	 <li><a href="index.php?action=admin&internships_state_filter=soumisEtPris&view_choice=internships_list_admin.php">Soumis et pris</a></li>
				  							 	 <li><a href="index.php?action=admin&internships_state_filter=libre&view_choice=internships_list_admin.php">Libre</a></li>
				  							 	 <li><a href="index.php?action=admin&internships_state_filter=attribue&view_choice=internships_list_admin.php">Attribué</a></li>
				  							 	 <li><a href="index.php?action=admin&internships_state_filter=complete&view_choice=internships_list_admin.php">Complété</a></li>
				  							 	 <li><a href="index.php?action=admin&internships_state_filter=exporte&view_choice=internships_list_admin.php">Exporté</a></li>
				  							 	 <li><a href="index.php?action=admin&internships_state_filter=supervise&view_choice=internships_list_admin.php">Supervisé</a></li>
				
											</ul>
									</div>
								</th>	
			         			
			                    <th><input type="text" class="form-control" name="first_name" placeholder="Prénom"></th>
			                    <th><input type="text" class="form-control" name="last_name" placeholder="Nom"></th>
			                    <th><input type="text" class="form-control" name="company" placeholder="Société"></th>
			           			<th>Premier Validateur</th>
			           			<th>Deuxième Validateur</th>
			  					<th>Superviseur</th>
			           			<th>Promoteur</th>
			           			<th>Contact interne</th>
								<th> <button type="submit" name="matchSupervisorButton" value='ok'  class="btn btn-primary">Superviser </button></th>
								<th><button type="submit" name="comfirmCompaniesButton" value='ok'  class="btn btn-success"> Valider </br> demandes </br>d'entreprises</button> </th>
								<th><button type="submit" name="comfirmStudentsButton" value='ok'  class="btn btn-success">  Valider </br> stages </br>étudiants</button> </th>
								<th> <button type="submit" name="exportInternshipAgreementButton" value='ok'  class="btn btn-success"> Exporter </br> convention PDF </button></th>
								<th> <button type="submit" name="exportInternshipButton" value='ok'  class="btn btn-success"> Exporter </br> stage PDF </button></th>
								<th><button type="submit" name="see_detailsButton"  value='ok' class="btn btn-primary">Détails </button></th>
								<th><button type="submit" name="grid_modeButton" value='ok' class="btn btn-primary">Modifier </button></th>
								<th><button type="submit" name="complete_modeButton"  value='ok' class="btn btn-warning">Faire </br>compléter</button></th>
			                </tr>
			            </thead>
			            <tbody>
			            	<?php if(empty($internships)) echo "Aucun résultat avec les filtres appliqués." ?>
			            	<?php foreach($internships as $key => $internship){ ?>

							<tr>
									<td><?php echo $internship['internship_state']; ?></td>
									<td><?php echo $internship['first_name_student']?></td>
									<td><?php echo $internship['last_name_student']?></td>
									<td><?php echo $internship['company_name']?></td>
									<td><?php echo $internship['first_validator']?></td>
									<td><?php echo $internship['second_validator']?></td>
									<td><?php echo $internship['supervisor']?></td>
									<td><?php echo $internship['promoter']?></td>
									<td><?php echo $internship['intern_contact']?></td>
									<td>
										<div class="form-group">
				  							<select class="form-control" id="sel1" name="supervisor_choice[]" <?php  if($internship['internship_state']!='Exporté' and $internship['internship_state']!='Supervisé' ) echo " disabled"?> >
				  									<option name="supervisor_choice[]" <?php if($internship['internship_state']=='Exporté') echo ' selected'?>>   /   </option>
				  								<?php foreach($teachers as $keyTeacher=>$teacher){?>
											    	<option name="supervisor_choice[]" <?php echo " value=".$keyTeacher.';'.$key ; if($internship['internship_state']=='Supervisé' and $internship['supervisor_email']==$keyTeacher) echo " selected ";?>><?php echo $teacher['teacherNames'];?></option>
											    <?php }?>
											  </select>
										</div>
									</td>
									<td class="bs-checkbox"> <input type="checkbox" name="validate_company_ask[]" value=<?php echo "".$internship['internship_id']?> <?php if($internship['internship_state']!="Soumis") echo " disabled"; ?> > </td>
									<td class="bs-checkbox"> <input type="checkbox" name="validate_students_proposition[]" value=<?php echo "".$internship['internship_id'];?>  <?php if($internship['internship_state']!="Soumis et pris") echo "disabled"; ?> > </td>
									<td class="bs-radio"> <input type="radio" name="exportateInternshipAgreement" value=<?php echo "".$internship['internship_id'];?>  <?php if($internship['internship_state']!="Complété" and $internship['internship_state']!="Exporté" and $internship['internship_state']!="Supervisé") echo "disabled"; ?> > </td>
									<td class="bs-radio"> <input type="radio" name="exportInternship" value=<?php echo "".$internship['internship_id'];?>  <?php if($internship['internship_state']!="Complété" and $internship['internship_state']!="Exporté" and $internship['internship_state']!="Supervisé") echo "disabled"; ?> > </td>
									<td class="bs-radio"> 
				             			<input type="radio" name="see_details" value=<?php echo $internship['internship_id']; ?> />
				             		</td>
				             		<td class="bs-radio" > 
				             			<input type="radio" name="grid_mode" value=<?php echo $internship['internship_id']; ?> />
				             		</td>
				             		<td class="bs-radio"> 
				             			<input type="radio" name="complete_mode" value=<?php echo $internship['internship_id']; if($internship['internship_state']!="Attribué") echo " disabled"?> />
				             		</td>
				             	</tr>
								
							<?php } ?>
		
			            </tbody>
			        </table>
				</div>
			</form>
		</div>
		<div class="col-md-2">
			<div id="RightSideBar">
				<div id="UserProfilBox">
					<h3 class="blockHeader">
						<span class="userName"><?php echo htmlentities($_SESSION['first_name'])." ".htmlentities($_SESSION['last_name']);   ?></span>
					</h3>
					<div id="userProfile">
						<div id="userPicture">
							<img class="userPicture" src="views/pictures/profilPicture.png" alt="Image de l'utilisateur">
						</div>
						<div id="userDetails">
							<p>
								<span class="bold">E-Mail :</span>
								<br><?php echo htmlentities($_SESSION['email']) ?>
							</p>
						
							<p> 
								<span class="bold">Fonction :</span>
								Responsable de stage
							</p>
							<form action="index.php?action=admin" method="POST">
								<p>
									<span class="bold">Date début projet : </span> <?php if(!$date_grid_mod) echo $config['dateBegin'] ; else echo "<input type=text class=\"form-control\" name=\"dateBegin\">"; ?>
								</p>

								<p>
									<span class="bold">Date fin projet : </span> <?php if(!$date_grid_mod) echo $config['dateEnd'] ; else echo "<input type=text class=\"form-control\" name=\"dateEnd\">"; ?>
								</p>
								<p>
									<span class="bold">Date butoire projet : </span> <?php if(!$date_grid_mod) echo $config['dateVeryEnd'] ; else echo "<input type=text class=\"form-control\" name=\"dateVeryEnd\">"; ?>
								</p>
								<p>
									<?php if(!$date_grid_mod) echo"<input type=\"submit\" name=\"date_grid_mod\" value=\"Modifier dates\" class=\"btn btn-warning\"/>";
										  else  echo"<input type=\"submit\" name=\"date_grid_mod\" value=\"Confirmer modifications\" class=\"btn btn-success\"/>"?>
				           
								</p>
							</form>
							<p>
								
							<form enctype="multipart/form-data" action="index.php?action=admin&view_choice=internships_list_admin.php" method="post">
									<label class="control-label">Uploader fichier étudiants :</label>
		     						<input type="hidden" name="MAX_FILE_SIZE" value="1000000000000" />
									<input type="file" name="userfile" class="btn"/>
									<input type="submit" class="btn btn-success"/>
								</form>
								<?php if(!empty($_FILES['userfile']))
									echo $messUpload;
								?>	
							</p>
					

							<p>
								<?php if(!$change_password) echo "
								<a href=\"index.php?action=admin&change_password=ok\">
									<img class=\"modif\" src=\"views/pictures/iconModif.png\" alt=\"Changer mon mot de passe personnel\">
										Changer mon mot de passe personnel
								</a>"; ?>
								<form action="index.php?action=admin&change_password=ok" method="POST">
								<?php if($change_password)	echo"
												<input class=\"form-control\" type=\"text\"  name=\"current_password\"  placeholder= \"Entrez votre mot de passe actuel\"/> 
												<input class=\"form-control\" type=\"password\"  name=\"first_password\"  placeholder=\"Entrez votre nouveau mot de passe\"/>
												<input class=\"form-control\" type=\"password\"  name=\"second_password\"  placeholder=\"Confirmer votre mot de passe\"/>
												<a class=\"btn btn-primary\" href=\"index.php?action=admin\"> Annuler </a>
												<input type=\"submit\" value=\"Changer mot de passe\" class=\"btn btn-warning\"/>";
								 ?> 
								 </form>
							</p>
							<p>
								<a href="index.php?action=supervisor" class="btn btn-link">Voir stages </br> personnels</a>	
								<a href="index.php?action=logout" class="btn btn-danger">Déconnexion</a>
							</p>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
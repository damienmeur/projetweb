<section id="formPage">
	<div class="row">
		<div class="col-sm-4">
			<div class="list-group" style="height:400px; overflow-y:scroll">
				<a class="list-group-item">
					
					<p class="list-group-item-text">Avez-vous trouvé un stage via la liste de propositions? Sélectionnez le ci-dessous pour pré-remplir le formulaire</p>
				</a> 
				<a href="#" class="list-group-item">
					<h4 class="list-group-item-heading">IBM</h4>
					<p class="list-group-item-text">Recherche stagiaire département
						sécurité</p>
					<p class="list-group-item-text">Région: Bruxelles</p>
				</a> <a href="#" class="list-group-item">
					<h4 class="list-group-item-heading">Microsoft</h4>
					<p class="list-group-item-text">Recherche stagiaire testing
						department</p>
					<p class="list-group-item-text">Région: Namur</p>
				</a> <a href="#" class="list-group-item">
					<h4 class="list-group-item-heading">Google</h4>
					<p class="list-group-item-text">Recherche stagiaire administrateur
						serveur</p>
					<p class="list-group-item-text">Région: Anvers</p>
				</a>
				<a href="#" class="list-group-item">
					<h4 class="list-group-item-heading">BridgeWater</h4>
					<p class="list-group-item-text">Recherche stagiaire marketing web</p>
					<p class="list-group-item-text">Région: Bruxelles</p>
				</a>
			</div>
		</div>

		<fieldset style=" height:400px; overflow-y:scroll">
			<form class="form-inline" action="index.php?action=form"<?php if($see_mode) echo "action=index.php?action=form&mode=see&internship_id=".$internship_to_see else echo" action=index.php?action=form"?>
				<h4 class="subtitle-form">Etudiant(e)</h4>
				<div class="form-group">
					<label for="exampleInputName2">Nom</label> <input type="text"
						class="form-control" id="exampleInputName2" <?php if($see_mode) echo "value=".$details['last_name_student'];?> >
				</div>
				<div class="form-group">
					<label for="exampleInputName2">Prénom</label> <input type="text"
						class="form-control" id="exampleInputName2" <?php if($see_mode) echo "value=".$details['first_name_student'];?>>
				</div>

				<div class="form-group">
					<label for="exampleInputEmail2">Mail</label> <input type="email"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=".$details['student_email'];?>>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">GSM</label> <input type="text"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=".$details['phone_student'];?>>
				</div>
				
				<h4 class="subtitle-form">Société Proposée</h4>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Dénomination</label> <input type="text"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=".$details['company_name'];?>>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Adresse</label> <input type="text"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=".$details['company_address'];?>>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Localité</label> <input type="text"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=".$details['company_locality'];?>>
				</div>
				
				<h4 class="subtitle-form">Personne de contact</h4>
				<div class="form-group">
					<label for="exampleInputName2">Nom</label> <input type="text"
						class="form-control" id="exampleInputName2" <?php if($see_mode) echo "value=".$details['ic_last_name'];?>
				</div>
				<div class="form-group">
					<label for="exampleInputName2">Prénom</label> <input type="text"
						class="form-control" id="exampleInputName2" <?php if($see_mode) echo "value=".$details['ic_first_name'];?>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Mail</label> <input type="email"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=".$details['ic_email'];?>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Service</label> <input type="text"
						class="form-control" id="exampleInputName2"
						<?php if($see_mode) echo "value=".$details['ic_service'];?>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Téléphone</label> <input type="text"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=".$details['ic_phone'];?>
				</div>
				
				<h4 class="subtitle-form">Informaticien disponible pour assurer l’encadrement “IT” du stage</h4>
				<div class="form-group">
					<label for="exampleInputName2">Nom</label> <input type="text"
						class="form-control" id="exampleInputName2" <?php if($see_mode) echo "value=".$details['promoter_last_name'];?>
				</div>
				<div class="form-group">
					<label for="exampleInputName2">Prénom</label> <input type="text"
						class="form-control" id="exampleInputName2" <?php if($see_mode) echo "value=".$details['promoter_first_name'];?>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Service</label> <input type="text"
						class="form-control" id="exampleInputName2"
						<?php if($see_mode) echo "value=".['promoter_service'];?>
				</div>
				
				<div class="form-group">
					<label for="exampleInputEmail2">Téléphone</label> <input type="text"
						class="form-control" id="exampleInputEmail2"
						<?php if($see_mode) echo "value=".['promoter_phone'];?>
				</div>
				
				<h5 class="subtitle-form">Description succincte du travail à effectuer :</h5>
					<p><?php if($see_mode) echo $details['description'];?></p>
				
				<h5 class="subtitle-form">Environnement de travail : logiciels, langages, technologies :</h5>
					<p><?php if($see_mode) echo $details['work_environment'];?></p>
				
				<h5 class="subtitle-form">D’ou vient la proposition (ecampus, relation, poursuite job, ...) ?</h5>
					<p><?php if($see_mode) echo $details['internship_origin'];?></p>;
				<?php if($grid_mod) echo '<button type="submit" class="btn btn-danger" value="submit">Modifier données</button';
				?>
				
			</form>
		</fieldset>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<a href=<?php if($see_mode) echo "index.php?action=admin"; else echo "index.php?action=student"  ?>>
				<img src="views/pictures/goBackButton.png" alt="Revenir en arrière">
			</a>
		</div>
	</div>
</section>
<section>
<?php if($error) echo "
		<div class=\"alert alert-danger\">
	  		<strong>Danger! </strong>" . $messError ."
		 </div>";
	?>
	<?php if($success) echo "
		<div class=\"alert alert-success\">
	  		<strong>OK ! </strong>" . $messSuccess ."
		 </div>";
	?>
	<div class="row">
		<div class="col-md-4">
			<div class="btn-demand">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Vous savez déjà où faire votre stage?</h3>
						<h3 class="panel-title">Faites directement une demande</h3>
					</div>
					<div class="panel-body">
						<a class="btn btn-default" href="index.php?action=form&mode=grid_mode_student">Faire une demande de stage</a>
					</div>
				</div>
			</div>
		</div>
		
		<div id="RightSideBarStudent">
			<div id="UserProfilBox">
				<h3 class="blockHeader">
					<span class="userName"><?php echo htmlentities($_SESSION['first_name']). " " .  htmlentities($_SESSION['last_name']) ?></span>
				</h3>
				<div id="userProfile">
					<div id="userPicture">
						<img class="userPicture" src="views/pictures/profilPicture.png"
							alt="Image de l'utilisateur">
					</div>
					<div id="userDetails">
						<p>
							<span class="bold">Matricule</span> <?php echo htmlentities($_SESSION['login']) ?>
						</p>
						
						<?php if($internship_state!=null){?>
						<p>
							<span class="bold">Etat de stage:</span> <?php echo $internship_state?>
						<p>
						<?php }?>
							<span class="bold">E-Mail</span> <br><?php echo htmlentities($_SESSION["email"])?>
						</p>
						<p>
							<span class="bold">Téléphone</span> -
						</p>
						<p>
							<span class="bold">Date début projet : </span> <?php echo $config['dateBegin'] ?>
						</p>

						<p>
							<span class="bold">Date Fin projet : </span> <?php echo $config['dateEnd'] ?>
						</p>
						<p>
							<span class="bold">Date Butoire projet :</span> <?php echo $config['dateVeryEnd']?>
						</p>
			
						<p>
								<?php if(!$change_password) echo "
								<a href=\"index.php?action=student&change_password=ok\">
									<img class=\"modif\" src=\"views/pictures/iconModif.png\" alt=\"Changer mon mot de passe personnel\">
										Changer mon mot de passe personnel
								</a>"; ?>
								<form action="index.php?action=student&change_password=ok" method="POST">
								<?php if($change_password)	echo"
												<input class=\"form-control\" type=\"text\"  name=\"current_password\"  placeholder= \"Entrez votre mot de passe actuel\"/> 
												<input class=\"form-control\" type=\"password\"  name=\"first_password\"  placeholder=\"Entrez votre nouveau mot de passe\"/>
												<input class=\"form-control\" type=\"password\"  name=\"second_password\"  placeholder=\"Confirmer votre mot de passe\"/>
												<a class=\"btn btn-primary\" href=\"index.php?action=student\"> Annuler </a>
												<input type=\"submit\" value=\"Changer mot de passe\" class=\"btn btn-warning\"/>";
								 ?> 
								 </form>
							</p>
						<p>

						</p>
						<p>
							<a href='index.php?action=logout' class='btn btn-danger'> Déconnexion</a>
						</p>
					</div>

				</div>
			</div>
		</div>
		<div class="col-md-6">
			<table class="table table-striped">
				<caption>Propositions de stage</caption>
				<thead>
					<tr>
						<th>Entreprise</th>
						<th>Description</th>
						<th>Région</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($internships as $internship_id => $internship){?>
					<tr>
						<td><?php echo $internship['company_name']?></td>
						<td><?php echo $internship['objectives']?></td>
						<td><?php echo $internship['locality']?></td>
					</tr>
					<?php }?>
					
				</tbody>
			</table>
		</div>

	</div>







</section>
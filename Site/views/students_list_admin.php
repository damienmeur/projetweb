 <section>
 	<?php if($error) echo "
		<div class=\"alert alert-danger\">
	  		<strong>Danger! </strong>" . $messError ."
		 </div>";
	?>
	<?php if($success) echo "
		<div class=\"alert alert-success\">
	  		<strong>OK ! </strong>" . $messSuccess ."
		 </div>";
	?>
<div class="row">
	<div class="col-md-10">
		<form action="index.php?action=admin&view_choice=students_list_admin.php" method="POST">
			<div class="btn-group btn-group-justified">
			  <a href="index.php?action=admin&view_choice=internships_list_admin.php" class="btn btn-primary">Stages</a>
			  <a href="#" class="btn btn-primary disabled">Etudiants</a>
			  <a href="index.php?action=admin&view_choice=teachers_list_admin.php" class="btn btn-primary">Professeurs</a>
			  <a href="index.php?action=admin&view_choice=contacts_list_admin.php" class="btn btn-primary">Contacts/promoteurs</a>
			  <a href="index.php?action=admin&view_choice=companies_list_admin.php" class="btn btn-primary">Sociétés</a>
			</div>
			<div class="panel panel-primary filterable">
		        <div class="panel-heading">
		            <h3 class="panel-title">Liste étudiants</h3>
		             <div class="pull-right">
				           <button type="submit"  class="btn btn-default "><span class="glyphicon glyphicon-filter"></span> Activer Filtres</button>
				      </div>
		        </div>
		        <table class="table">
		            <thead>
		                <tr class="filters">

		                     <th>
			           	     	<div class="dropdown">
			  						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Etat du stage<span class="caret"></span></button>
			  							<ul class="dropdown-menu">
			  								<li><a href="index.php?action=admin&internships_state_filter=tous&view_choice=students_list_admin.php">Tous</a></li>
			  							 	 <li><a href="index.php?action=admin&internships_state_filter=soumisEtPris&view_choice=students_list_admin.php">Soumis et pris</a></li>
			  							 	 <li><a href="index.php?action=admin&internships_state_filter=attribue&view_choice=students_list_admin.php">Attribué</a></li>
			  							 	 <li><a href="index.php?action=admin&internships_state_filter=complete&view_choice=students_list_admin.php">Complété</a></li>
			  							 	 <li><a href="index.php?action=admin&internships_state_filter=exporte&view_choice=students_list_admin.php">Exporté</a></li>
			  							 	 <li><a href="index.php?action=admin&internships_state_filter=supervise&view_choice=students_list_admin.php">Supervisé</a></li>
			
										</ul>
								</div>
							</th>
		                    <th><input type="text" class="form-control" name="first_name" placeholder="Prénom"></th>
				            <th><input type="text" class="form-control" name="last_name" placeholder="Nom"></th>
		                    <th>E-mail</th>
		                    <th>N° Gsm</th>
		                    <th><button type="submit" name="see_details_studentsButton" value="ok" class="btn btn-primary">Détails du stage </button></th>
							<th><button type="submit" name="modify_student_infoButton" value="ok" class="btn btn-warning">Modifier info étudiant</button></th>
		                </tr>
		            </thead>
		            <tbody>
		            	<?php if(empty($students)) echo "Aucun résultat avec les filtres appliqués." ?>
		            	<?php foreach($students as $key => $student){ ?>
			             	<tr>
			             		<td><?php echo  $student['internship_state']; ?></td>
			             		<?php  if($registration_id_to_modify==$key) echo "<input type=\"hidden\" name=\"registration_id\" value=".$key."/>"?>
								<td><?php  if($registration_id_to_modify==$key) echo"<input class=\"form-control\" type=\"text\" name=\"student_first_name\" value=".$student['first_name']." />"; else echo $student['first_name']?></td>
								<td><?php if($registration_id_to_modify==$key) echo"<input class=\"form-control\" type=\"text\" name=\"student_last_name\" value=".$student['last_name']." />"; else echo $student['last_name']?></td>
								<td><?php if($registration_id_to_modify==$key) echo"<input class=\"form-control\" type=\"text\" name=\"student_email\" value=".$student['email']." />"; else echo $student['email']?></td>
								<td><?php if($registration_id_to_modify==$key) echo"<input class=\"form-control\" type=\"text\" name=\"student_phone\" value=".$student['phone']." />"; else echo $student['phone']?></td>
								<td class="bs-radio"> 
				             		<input type="radio" name="see_student_details" value=<?php echo $key; if($student['internship_state']=='Pas de stage') echo " disabled" ?> />
				             	</td>
				             	<?php if($registration_id_to_modify!=$key) echo "
									<td class=\"bs-radio\">
					             		<input type=\"radio\" name=\"modify_student_details\" value=".$key."  />
					             	</td>";
					             	 	else echo "
					             	 		<td>
					             	 			<input class=\"btn btn-danger\" type=\"submit\" value=\"Confirmer\" />
					             	 		</td>";?>
							</tr>
						<?php } ?>
						
		            </tbody>
		        </table>
			</div>
		</form>
	</div>
	<div class="col-md-2">
			<div id="RightSideBar">
				<div id="UserProfilBox">
					<h3 class="blockHeader">
						<span class="userName"><?php echo htmlentities($_SESSION['first_name'])." ".htmlentities($_SESSION['last_name']);   ?></span>
					</h3>
					<div id="userProfile">
						<div id="userPicture">
							<img class="userPicture" src="views/pictures/profilPicture.png" alt="Image de l'utilisateur">
						</div>
						<div id="userDetails">
							<p>
								<span class="bold">E-Mail :</span>
								<br><?php echo htmlentities($_SESSION['email']) ?>
							</p>
						
							<p> 
								<span class="bold">Fonction :</span>
								Responsable de stage
							</p>
							<form action="index.php?action=admin" method="POST">
								<p>
									<span class="bold">Date début projet : </span> <?php if(!$date_grid_mod) echo $config['dateBegin'] ; else echo "<input type=text class=\"form-control\" name=\"dateBegin\">"; ?>
								</p>

								<p>
									<span class="bold">Date début projet : </span> <?php if(!$date_grid_mod) echo $config['dateEnd'] ; else echo "<input type=text class=\"form-control\" name=\"dateEnd\">"; ?>
								</p>
								<p>
									<span class="bold">Date début projet : </span> <?php if(!$date_grid_mod) echo $config['dateVeryEnd'] ; else echo "<input type=text class=\"form-control\" name=\"dateVeryEnd\">"; ?>
								</p>
								<p>
									<?php if(!$date_grid_mod) echo"<input type=\"submit\" name=\"date_grid_mod\" value=\"Modifier dates\" class=\"btn btn-warning\"/>";
										  else  echo"<input type=\"submit\" name=\"date_grid_mod\" value=\"Confirmer modifications\" class=\"btn btn-success\"/>"?>
				           
								</p>
							</form>
							<p>
								
								<form enctype="multipart/form-data" action="index.php?action=admin&view_choice=internships_list_admin.php" method="post">
									<label class="control-label">Uploader fichier étudiants :</label>
		     						<input type="hidden" name="MAX_FILE_SIZE" value="1000000000000" />
									<input type="file" name="userfile" class="btn"/>
									<input type="submit" class="btn btn-success"/>
								</form>
								<?php if(!empty($_FILES['userfile']))
									echo $messUpload;
								?>	
							</p>
					

							<p>
								<?php if(!$change_password) echo "
								<a href=\"index.php?action=admin&change_password=ok&view_choice=students_list_admin.php\">
									<img class=\"modif\" src=\"views/pictures/iconModif.png\" alt=\"Changer mon mot de passe personnel\">
										Changer mon mot de passe personnel
								</a>"; ?>
								<form action="index.php?action=admin&change_password=ok" method="POST">
								<?php if($change_password)	echo"
												<input class=\"form-control\" type=\"text\"  name=\"current_password\"  placeholder= \"Entrez votre mot de passe actuel\"/> 
												<input class=\"form-control\" type=\"password\"  name=\"first_password\"  placeholder=\"Entrez votre nouveau mot de passe\"/>
												<input class=\"form-control\" type=\"password\"  name=\"second_password\"  placeholder=\"Confirmer votre mot de passe\"/>
												<a class=\"btn btn-primary\" href=\"index.php?action=admin&view_choice=students_list_admin.php\"> Annuler </a>
												<input type=\"submit\" value=\"Changer mot de passe\" class=\"btn btn-warning\"/>";
								 ?> 
								 </form>
							</p>
							<p>
								<a href="index.php?action=supervisor" class="btn btn-link">Voir stages </br> personnels</a>	
								<a href="index.php?action=logout" class="btn btn-danger">Déconnexion</a>
							</p>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
</section>
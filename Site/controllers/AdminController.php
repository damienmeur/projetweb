<?php

class AdminController{
	public function __controller(){
		
	}
	
	public function run(){

		
		$exportate=false;

		if(empty($_SESSION["adminAuthentification"]) or $_SESSION["adminAuthentification"]==false ){
			header('location: index.php?action=login');
			die();
		}
		if(!empty($_GET['see_own'])){
			$_GET=array();
			header('location: index.php?action=supervisor');
			die();
		}
		if(!empty($_POST["see_detailsButton"])){
			if(!empty($_POST["see_details"])) {
				$internship_to_see=$_POST["see_details"];
				header('location: index.php?action=form&mode=see_mode_admin&internship_id='.$internship_to_see);
				die();
			}
		}
		if(!empty($_POST["grid_modeButton"])){
			if(!empty($_POST["grid_mode"])) {
			$internship_to_see=$_POST["grid_mode"];
			header('location: index.php?action=form&mode=grid_mode_admin&internship_id='.$internship_to_see);
			die();
		}
		}
		
		if(!empty($_POST["complete_modeButton"])) {
			if(!empty($_POST["complete_mode"])) {
				$internship_to_see=$_POST["complete_mode"];
				header('location: index.php?action=form&mode=complete_mode_promoter&internship_id='.$internship_to_see);
				die();
			}
		}
		
		require 'models/config.properties.php';

		$change_password=false;
		$error=false;
		$success=false;
		if(sha1($config['teachers_default_pswd'])==$_SESSION["password"]){
			$change_password=true;
			$error=true;
			$messError="Votre mot de passe est celui par défaut, vous devriez le changer le plus vite possible.";
		}
		
		if(!empty($_GET["change_password"])){
			$messPassword="";
			$change_password=true;
			if(empty($_POST["current_password"]) or empty($_POST["first_password"]) or empty($_POST["second_password"])){
				$error=true;
				$messError="Un des champs n'est pas rempli";
			}
			if(!empty($_POST["current_password"]) and !empty($_POST["first_password"]) and !empty($_POST["second_password"])){
				if(sha1($_POST["current_password"])!=$_SESSION["password"]){
					$error=true;
					$messError="Le mot de passe est incorrect";
				}
				elseif($_POST["first_password"]!=$_POST["second_password"]){
					$error=true;
					$messError="Les mots-de-passe ne correspondent pas";
				}
				else{
					$success=true;
					Db::getInstance()->updatePasswordTeacher($_POST["first_password"], $_SESSION["login"]);
					$_SESSION["password"]=sha1(htmlentities($_POST["first_password"]));
					$messSuccess="Le mot de passe a correctement été changé";
				}
			}

		}
		if(!empty($_GET["modifiyInternship"]) and $_GET["modifiyInternship"]=="success"){
			$success=true;
			$messSuccess="Modification dans la base de donnée bien effectuée";
		}

		$date_grid_mod=false;
		if(!empty($_POST["date_grid_mod"])){
			if($_POST["date_grid_mod"]=="Modifier dates") $date_grid_mod=true;
			if($_POST["date_grid_mod"]=="Confirmer modifications"){
				if(!empty($_POST["dateBegin"]) and !empty($_POST["dateEnd"]) and !empty($_POST["dateVeryEnd"]) ){
					$dateBegin=$_POST["dateBegin"];
					$dateEnd=$_POST["dateEnd"];
					$dateVeryEnd=$_POST["dateVeryEnd"];
					$date_grid_mod=true;
					$configPath='models/config.properties.php';
					// load the data and delete the line from the array 
					$lines = file($configPath); 
					$last = sizeof($lines) - 1 ; 
					unset($lines[$last]); 

					// write the new data to the file 
					$fp = fopen($configPath, 'w'); 
					fwrite($fp, implode('', $lines));
					fwrite($fp,"\$config['dateBegin']=\"".$dateBegin."\";\n");
					fwrite($fp,"\$config['dateEnd']=\"".$dateEnd."\";\n");
					fwrite($fp,"\$config['dateVeryEnd']=\"".$dateVeryEnd."\";\n");
					fwrite($fp,"return \$config; ?>");
					fclose($fp); 
					$date_grid_mod=false;
				
				}	
			}
		}
		if(!empty($_POST["comfirmCompaniesButton"])){
			if(!empty($_POST["validate_company_ask"])){
				foreach($_POST["validate_company_ask"] as $key=>$internship_id){
					if(Db::getInstance()->isPrevalidate($internship_id,$_SESSION["email"])=="canBePrevalidate"){
						Db::getInstance()->preValidate($internship_id,$_SESSION["email"]);
						$success=true;
						$messSuccess="Demande de stagiaire d'entreprise prévalidée";
						break;
					}
					if(Db::getInstance()->isPrevalidate($internship_id,$_SESSION["email"])=="canBeValidate"){
						Db::getInstance()->validateCompanyAsk($internship_id,$_SESSION["email"]);
							$success=true;
							$messSuccess="Demande de stagiaire d'entreprise validée";
						break;
					}	
				}
			}
		}
		if(!empty($_POST["comfirmStudentsButton"])){
			if(!empty($_POST["validate_students_proposition"])){

				foreach($_POST["validate_students_proposition"] as $key=>$internship_id){
					if(Db::getInstance()->isPrevalidate($internship_id,$_SESSION["email"])=="canBePrevalidate"){
						Db::getInstance()->preValidate($internship_id,$_SESSION["email"]);
						$sucess=true;
						$messSuccess="Proposition de stage par un étudiant prévalidée";
						break;
					}
					if(Db::getInstance()->isPrevalidate($internship_id,$_SESSION["email"])=="canBeValidate"){
						Db::getInstance()->validateStudentProposition($internship_id,$_SESSION["email"]);
						$sucess=true;
						$messSuccess="Proposition de stage par un étudiant validée";
						break;
					}	
				}			
			}
		}

		if(!empty($_POST["exportInternshipAgreementButton"])){
			if(!empty($_POST["exportateInternshipAgreement"])){
				$internship_id=$_POST["exportateInternshipAgreement"];
				header("location: index.php?action=exportate&type=convention&internship_id=".$internship_id);
				die();
			}
		}
		if(!empty($_POST["exportInternshipButton"])){
			if(!empty($_POST["exportInternship"])){
				$exportate=true;
				$internship_id=$_POST["exportInternship"];
				$details=Db::getInstance()->getInternshipDetails($internship_id);
				if($details['internship_state']=="Complété") {
					Db::getInstance()->setExportate($internship_id);
				}
				header("location: index.php?action=exportate&type=internship&internship_id=".$internship_id);
				die();

			}
		}
		
		
		if(!empty($_GET["exportInternships"])){
			header("location: index.php?action=exportate&type=internships");
			die();
		}
		if(!empty($_POST["matchSupervisorButton"])){
			if(!empty($_POST["supervisor_choice"])){
				$supervisor_choice=array();
				$supervisor_choice=$_POST["supervisor_choice"];
				foreach ($supervisor_choice as $key => $choice) {
					$matches=array();
					preg_match('/(.*);(.*)/',$choice,$matches);
					if(count($matches)!=0){
						$internship_id=$matches[2];
						if($internship_id=="/") break;
						$new_supervisor_email=$matches[1];
						Db::getInstance()->superviseInternship($internship_id,$new_supervisor_email);
					}			
				}
			}	
		}
	
		if(!empty($_GET["successUploadExport"])){
			$success=true;
			$messSuccess="Fichiers pdf bien exportés dans la racine du serveur";
		}
		if(!empty($_POST["see_details_studentsButton"])){
			if(!empty($_POST["see_student_details"])){
				$registration_id=$_POST["see_student_details"];
				$internship_to_see=(int) Db::getInstance()->getInternshipId($registration_id);
				header('location: index.php?action=form&mode=see_mode_admin&internship_id='.$internship_to_see);
				die();
			}
		}
		
		
		$registration_id_to_modify="////";
		if(!empty($_POST["modify_student_infoButton"])){
			
			if(!empty($_POST["modify_student_details"])){
				$registration_id_to_modify=$_POST["modify_student_details"];		
			}
		}
		
		//modifications for students
		if(!empty($_POST["student_first_name"])){
			$student_first_name=htmlentities($_POST["student_first_name"]);
			$student_last_name=htmlentities($_POST["student_last_name"]);
			$student_email=htmlentities($_POST["student_email"]);
			$student_phone=htmlentities($_POST["student_phone"]);

			$registration_id=(int)$_POST["registration_id"];
			Db::getInstance()->updateStudentInfo($registration_id,$student_first_name,$student_last_name,$student_email,$student_phone);
			$success=true;
			$messSuccess="L'étudiant a bien été modifié dans la base de données";
		}
		//modifications for teachers
		$email_unique_id_to_modify="////";
		if(!empty($_POST['modify_teacher_info'])){
			if(!empty($_POST["modify_teacher_details"]))
				$email_unique_id_to_modify=$_POST["modify_teacher_details"];		
		}
		
		if(!empty($_POST["confirm_modify_teacher"])){
			if(!empty($_POST["teacher_function"])){
				$email_unique_id=$_POST["email_unique_id_to_modify"];
				$teacher_function=htmlentities($_POST["teacher_function"]);
				$is_admin=1;
				if($teacher_function=="Superviseur") $is_admin=0;
				Db::getInstance()->updateTeacherFunction($email_unique_id,$is_admin);
				$success=true;
				$messSuccess="Modification dans la base de donnée effectuée";
			}
		}
		//delete teacher
		$email_unique_id_to_delete="////qsdcqscq";
		if(!empty($_POST['delete_teacher_info'])){
			if(!empty($_POST["set_unactive"]))
				$email_unique_id_to_delete=$_POST["set_unactive"];		
		}
		
		if(!empty($_POST["comfirm_delete_teacher"])){
			if(!empty($_POST["email_unique_id_to_delete"])){
				$email_unique_id=$_POST["email_unique_id_to_delete"];
				$haveInternship=false;
				if(Db::getInstance()->getAmountInternshipsTeachers($email_unique_id)==0){
					Db::getInstance()->setInactiveTeacher($email_unique_id);
					$success=true;
					$messSuccess="Modification dans la base de donnée effectuée";
				}
				else {
					$error=true;
					$messError="Vous ne pouvez pas supprimer un professeur qui supervise un stage, supervisez le stage avec un autre professeur et recommencez";
				}
				
			}
		}
		$teachers=Db::getInstance()->getAllTeachersCSV();
		//export csv teachers
		if(!empty($_GET["exportTeachers"])){
			$teachers=Db::getInstance()->getAllTeachersCSV();
			ob_clean();
			$fichier = new ExcelFile();
			$fichier->Colonne("Email;Nom;Prenom;Responsable");
			foreach($teachers as $email_unique_id=>$teacher){
				$fichier->Insertion($teacher);
			}
			ob_flush();
			$fichier->output('professeurs');


		}
     		
  
  		
		//modifications for contacts
		$intern_contact_id_to_modify="////qsdDFVSDFGV";
		if(!empty($_POST["modify_contact_infoButton"])){
			if(!empty($_POST["modify_contact_details"])){
				$intern_contact_id_to_modify=$_POST["modify_contact_details"];		
			}
		}
		if(!empty($_POST["contact_first_name"])){
				$intern_contact_id=(int)$_POST["intern_contact_id"];
				$contact_first_name=htmlentities($_POST["contact_first_name"]);
				$contact_last_name=htmlentities($_POST["contact_last_name"]);
				$contact_email=htmlentities($_POST["contact_email"]);
				$contact_phone=htmlentities($_POST["contact_phone"]);
				$contact_service=htmlentities($_POST["contact_service"]);
				$contact_function=htmlentities($_POST["contact_function"]);
				Db::getInstance()->updateContactInfo($intern_contact_id,$contact_first_name,$contact_last_name,$contact_email,$contact_phone,$contact_service,$contact_function);
				$success=true;
				$messSuccess="Les informations du contact ont correctement été modifiées dans la base de données";

		}
		//modificaition for companies
		$company_id_to_modify="////qsdDFVSDFGVqsdfc";
		if(!empty($_POST["modify_company_infoButton"])){
			if(!empty($_POST["modify_company_infoButton"])){
				$company_id_to_modify=$_POST["modify_company_details"];		
			}
		}
		if(!empty($_POST["company_name"])){
				$company_id=(int)$_POST["company_id"];
				$company_name=htmlentities($_POST["company_name"]);
				$company_address=htmlentities($_POST["company_address"]);
				$company_phone=htmlentities($_POST["company_phone"]);
				$company_zip=htmlentities($_POST["company_zip"]);
				$company_locality=htmlentities($_POST["company_locality"]);
				Db::getInstance()->updateCompanyInfo($company_id,$company_name,$company_address,$company_phone,$company_zip,$company_locality);
				$success=true;
				$messSuccess="Les informations de la société ont correctement été modifiées dans la base de données";
		}
		
		$view_choice="internships_list_admin.php";
		
		if(!empty($_GET["view_choice"])){
			$view_choice=$_GET["view_choice"];
		}
		if($view_choice=='internships_list_admin.php'){
			$internship_state='tous';
			$last_name='';
			$first_name='';
			$company='';
			if(!empty($_GET["internships_state_filter"])) $internship_state=$_GET["internships_state_filter"];
			if(!empty($_POST["last_name"])) $last_name=$_POST["last_name"];
			if(!empty($_POST["first_name"])) $first_name=$_POST["first_name"];
			if(!empty($_POST["company"])) $company=$_POST["company"];
			$internships=array();
			$internships=Db::getInstance()->selectInternshipsWithFilters($internship_state,$last_name,$first_name,$company);
			$teachers=Db::getInstance()->getAllTeachers();
		}
		if($view_choice=='students_list_admin.php'){
			$internship_state='tous';
			$last_name='';
			$first_name='';
			if(!empty($_GET["internships_state_filter"])) $internship_state=$_GET["internships_state_filter"];
			if(!empty($_POST["last_name"])) $last_name=$_POST["last_name"];
			if(!empty($_POST["first_name"])) $first_name=$_POST["first_name"];
			$students=array();
			$students=Db::getInstance()->selectStudentsWithFilters($internship_state,$first_name,$last_name);
		}
		if($view_choice=='teachers_list_admin.php'){
			$last_name='';
			$first_name='';
			if(!empty($_POST["last_name"])) $last_name=$_POST["last_name"];
			if(!empty($_POST["first_name"])) $first_name=$_POST["first_name"];
			$teachers=array();
			$teachers=Db::getInstance()->selectTeachersWithFilters($first_name,$last_name);
		}
		if($view_choice=='contacts_list_admin.php'){
			$last_name='';
			$first_name='';
			if(!empty($_POST["last_name"])) $last_name=$_POST["last_name"];
			if(!empty($_POST["first_name"])) $first_name=$_POST["first_name"];
			$contacts=array();

			$contacts=Db::getInstance()->selectContactsWithFilters($first_name,$last_name);
			
		}
		if($view_choice=='companies_list_admin.php'){
			$company_name='';
			if(!empty($_POST["company_name"])) $company_name=$_POST["company_name"];
			$companies=array();
			$companies=Db::getInstance()->selectCompaniesWithFilters($company_name);
			
		}
		
		if (!empty($_FILES['userfile'])){
			$messUpload=self::fileUpload();
			if(empty(Db::getInstance()->select_student())){
				self::setAllStudents('models/etudiants.csv');
			}	
		}


		$config=require 'models/config.properties.php';
		require_once(CHEMIN_VUES .$view_choice);
	}
	private static function setAllStudents($csvfile) {
		if (file_exists ($csvfile )) {
			$fcontents = file ( $csvfile );
			foreach ( $fcontents as $i => $icontent ) {
				if($i!=0){
					preg_match ( '/(.*);(.*);(.*);(.*)/', $icontent, $result );
					require 'models/config.properties.php';
					Db::getInstance ()->insert_student($result [1], 'true', $result[3] , $result[2], $result[4] , null , $config['students_default_pswd']);
	
				}
	
			}
		}
	}
	
	private static function fileUpload(){
		
		$messUpload = "";
		if (!empty($_FILES['userfile']['tmp_name'])) {
			$origin= $_FILES['userfile']['tmp_name'];
			$repertoireDestination = 'models/';
			$destination        = $repertoireDestination."etudiants.csv";
			move_uploaded_file($origin, $destination);
			return $messUpload = "Le fichier a bien été importé";
		
		
		}else{
		return $messUpload;
		}
	}
	
}
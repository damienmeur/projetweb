<?php
class FormController {
	public function __controller() {
	}
	public function run() {
		$grid_mode = false;
		$complete_mode = false;
		$supervisor_mode = false;
		$admin = false;
		$isFirst = true;
		$pre_encoded = false;
		
		if (! empty ( $_GET ["mode"] )) {
			$details = array ();
			if ($_GET ["mode"] == "see_mode_admin" or $_GET ["mode"] == "grid_mode_admin" or $_GET ["mode"] == "complete_mode_promoter")
				$admin = true;
			
			if ($admin and ! empty ( $_GET ["internship_id"] )) {
				$internship_to_see = ( int ) htmlentities ( $_GET ["internship_id"] );

				$details = Db::getInstance ()->getInternshipDetails ( $internship_to_see );

				if ($_GET ["mode"] == "grid_mode_admin") {
					$grid_mode = true;
				}
				if ($_GET ["mode"] == "complete_mode_promoter") {
					$complete_mode = true;
				}
			}
			if ($_GET ["mode"] == "see_mode_supervisor") {
				$supervisor_mode = true;
				$internship_to_see = ( int ) htmlentities ( $_GET ["internship_id"] );
				$details = Db::getInstance ()->getInternshipDetails ( $internship_to_see );
			}
			
			if ($_GET ["mode"] == "grid_mode_student") {
				
				if (empty ( $_GET ["validate"] ) and ! empty ( $_GET ["internship_id"] )) {
					$internship_to_see = htmlentities ( $_GET ["internship_id"] );
					$details = Db::getInstance ()->getInternshipDetails ( $internship_to_see );


					$isFirst = false;
				} 
				elseif (empty ( $_GET ["validate"] ) and empty ( $_GET ["internship_id"] )) {

					$details ['company_name'] = "/";
					$details ['company_address'] = "/";
					$details ['company_locality'] = "/";
					$details ['company_zip'] = "/";
					$details ['promoter_first_name'] = "/";
					$details ['promoter_last_name'] = "/";
					$details ['promoter_service'] = "/";
					$details ['promoter_phone'] = "/";
					$details ['promoter_email'] = "/";
					$details ['ic_first_name'] = "/";
					$details ['ic_last_name'] = "/";
					$details ['ic_service'] = "/";
					$details ['ic_phone_number'] = "/";
					$details ['ic_email'] = "/";
					$details ['description'] = "/";
					$details ['remarks'] = "/";
					$details ['work_environment'] = "/";
					$details ['internship_origin'] = "/";
					$details ['secretaryship_phone'] = "/";
					$details ['ic_service'] = "/";
					$details ['ic_function'] = "/";
					$details ['promoter_service'] = "/";
				}
				$studentInfo = Db::getInstance ()->getStudentDetails ( $_SESSION ["login"] );
				$details ['first_name_student'] = $studentInfo ['first_name'];
				$details ['last_name_student'] = $studentInfo ['last_name'];
				$details ['student_email'] = $studentInfo ['email'];
				$details ['student_phone'] = $studentInfo ['phone_number'];

				$grid_mode = true;
			}
			
			if (! empty ( $_GET ["validate"] )) {
	
				if (! empty ( $_GET ["internship_id"] )){
					$internship_to_see = ( int ) $_GET ["internship_id"];
					$details=Db::getInstance()->getInternshipDetails($internship_to_see);
					$details['internship_id']=$internship_to_see;
	
				}
				if ($_GET ["mode"] == "pre_encoded_mode") {
					$pre_encoded = true;
				} 
				elseif ($_GET ["mode"] == "complete_mode_promoter")
					$complete_mode = true;

			
				foreach ( $_POST as $key => $value ) {
					$details [$key] = $value;
				}
				$details['remarks']="/";
				$details['objectives']="/";
				
				if (! $pre_encoded and ! $admin) {
					$details ['internship_state'] = "soumisEtPris";
				} elseif ($pre_encoded and ! $admin) {
					$details ['internship_id'] = $internship_to_see;
					$details ['internship_state'] = "attribue";
					$details ['registration_id'] = ( int ) $_SESSION ["login"];
				}
			
				if (Db::getInstance ()->get_internship_state ( Db::getInstance ()->getInternshipId ( $_SESSION ['login'] ) ) === NULL) {
					  if ($admin) {
						if ($complete_mode) {
						  $details ['internship_state'] = 'complete';
					  }
						 Db::getInstance ()->setOrUpdateInternship ( $details );
				
					  header ( "location: index.php?action=admin&modifiyInternship=success" );
					  die ();
					  } else {
					 	Db::getInstance ()->setOrUpdateInternship ( $details );
					  header ( "location: index.php?action=student&addInternship=success" );
					  die ();
					  }
					 
				} else {
					header ( "location: index.php?action=student&addInternship=error" );
					die ();
				}
			}
		}
		$see_mode = true;
		$internships = Db::getInstance ()->select_company_table ();
		// //////EN CAS DE SELECTION D'UN STAGE DANS LA FORM VIEW////////
		if (! empty ( $_POST ['id_stage'] )) {
			$pre_encoded = true;
			$isFirst = false;
			$details = Db::getInstance ()->getInternshipDetails ( ( int ) $_POST ['id_stage'] );
			$studentInfo = Db::getInstance ()->getStudentDetails ( $_SESSION ["login"] );
			$details ['first_name_student'] = $studentInfo ['first_name'];
			$details ['last_name_student'] = $studentInfo ['last_name'];
			$details ['student_email'] = $studentInfo ['email'];
			$details ['student_phone'] = $studentInfo ['phone_number'];
			$internship_to_see = $_POST ['id_stage'];
		}
		
		require_once (CHEMIN_VUES . "form.php");
	}
}
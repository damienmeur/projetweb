<?php

class SupervisorController{
	public function __controller(){
		
	}
	
	public function run(){
		if( empty($_SESSION['supervisorAuthentification']) ){
			header('location : index.php?action=login');
			die();
		}
		require 'models/config.properties.php';

		$change_password=false;
		$error=false;
		$success=false;
		if(sha1($config['teachers_default_pswd'])==$_SESSION["password"]){
			$change_password=true;
			$error=true;
			$messError="Votre mot de passe est celui par défaut, vous devriez le changer le plus vite possible.";
		}
		if(!empty($_POST["see_details"])){
			$internship_to_see=$_POST["see_details"];
			header('location: index.php?action=form&mode=see_mode_supervisor&internship_id='.$internship_to_see);
			die();
		}
		
		if(!empty($_GET["change_password"])){
			$messPassword="";
			$change_password=true;
			if(empty($_POST["current_password"]) or empty($_POST["first_password"]) or empty($_POST["second_password"])){
				$error=true;
				$messError="Un des champs n'est pas rempli";
			}
			if(!empty($_POST["current_password"]) and !empty($_POST["first_password"]) and !empty($_POST["second_password"])){
				if(sha1($_POST["current_password"])!=$_SESSION["password"]){
					$error=true;
					$messError="Le mot de passe est incorrect";
				}
				elseif($_POST["first_password"]!=$_POST["second_password"]){
					$error=true;
					$messError="Les mots-de-passe ne correspondent pas";
				}
				else{
					$success=true;
					Db::getInstance()->updatePasswordTeacher($_POST["first_password"], $_SESSION["login"]);
					$_SESSION["password"]=sha1(htmlentities($_POST["first_password"]));
					$messSuccess="Le mot de passe a correctement été changé";
				}
			}

		}
		$exportate=false;
		if(!empty($_POST["exportateInternships"])){
			$exportate=true;
			$internship_id=$_POST["exportateInternships"];
			$details=Db::getInstance()->getInternshipDetails($internship_id);
			header("location: index.php?action=exportate&type=internship&internship_id=".$internship_id);
			die();

		}
		$internship_state='tous';
		$last_name='';
		$first_name='';
		$company_name='';
		if(!empty($_GET["internships_state_filter"])) $internship_state=$_GET["internships_state_filter"];
		if(!empty($_POST["last_name"])) $last_name=$_POST["last_name"];
		if(!empty($_POST["first_name"])) $first_name=$_POST["first_name"];
		if(!empty($_POST["company_name"])) $company_name=$_POST["company_name"];
		$internships=array();
		$internships=Db::getInstance()->selectInternshipsWithFiltersSupervisor($internship_state,$last_name,$first_name,$company_name, $_SESSION['email']);


		$config=require 'models/config.properties.php';
		require_once(CHEMIN_VUES . "supervisor.php");
	}
	
}
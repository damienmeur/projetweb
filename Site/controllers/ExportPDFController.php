<?php

class ExportPDFController{
	
	public function __controller(){
		
	}
	
	public function run(){
		require ('\lib\tcpdf\tcpdf.php');
	

		// create some HTML content
		if($_GET["type"]=="convention"){
			$internship_id=(int)$_GET["internship_id"];
			self::exportAgreement($internship_id);

		}
		if($_GET["type"]=="internship"){
			$internship_id=(int)$_GET["internship_id"];
			self::exportInternship($internship_id);
			
		}
		if($_GET["type"]=="internships"){
			self::exportInternships();
			
		
			
		}
		

	}
	private static function exportInternship($internship_id){
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->AddPage();
		$details=Db::getInstance()->getInternshipDetails($internship_id);
		$html = file_get_contents(CHEMIN_VUES . 'internship_export.php');
		$html = str_replace("#company_name#",$details['company_name'],$html);
		$html = str_replace("#student_first_name#",$details['first_name_student'],$html);
		$html = str_replace("#student_last_name#",$details['last_name_student'],$html);
		$html = str_replace("#promoter_first_name#",$details['promoter_first_name'],$html);
		$html = str_replace("#promoter_last_name#",$details['promoter_last_name'],$html);
		$html = str_replace("#company_address#",$details['company_address'],$html);
		$html = str_replace("#company_locality#",$details['company_locality'],$html);
		$html = str_replace("#student_email#",$details['student_email'],$html);
		$html = str_replace("#student_phone#",$details['student_phone'],$html);
		$html = str_replace("#promoter_service#",$details['promoter_service'],$html);
		$html = str_replace("#promoter_phone#",$details['promoter_phone'],$html);
		$html = str_replace("#promoter_email#",$details['promoter_email'],$html);
		$html = str_replace("#ic_first_name#",$details['ic_first_name'],$html);
		$html = str_replace("#ic_last_name#",$details['ic_last_name'],$html);
		$html = str_replace("#ic_service#",$details['ic_service'],$html);
		$html = str_replace("#ic_phone_number#",$details['ic_phone_number'],$html);
		$html = str_replace("#ic_email#",$details['ic_email'],$html);
		$html = str_replace("#description#",$details['description'],$html);
		$html = str_replace("#remarks#",$details['remarks'],$html);
		$html = str_replace("#objectives#",$details['objectives'],$html);
		$html = str_replace("#work_environment#",$details['work_environment'],$html);
		$html = str_replace("#internship_origin#",$details['internship_origin'],$html);
		$html = str_replace("#company_zip#",$details['company_zip'],$html);
		$html .= '<style>'.file_get_contents(CHEMIN_VUES.'bootstrap/css/styleExport.css').'</style>';

		// output the HTML content
		$pdf->writeHTML($html, true, 0, true, 0);

		// reset pointer to the last page
		$pdf->lastPage();


		# Un contrôleur se termine en écrivant une vue qui est ici un PDF !
		//Close and output PDF document
		ob_end_clean();
		$pdf->Output('export_'.$details['first_name_student'].'_'.$details['last_name_student'].'.pdf', 'I');

	}
	private static function exportInternships(){
		
		$internships=Db::getInstance()->selectexportateAndSupervisedInternshipIds();
		foreach ($internships as  $key=> $internship_id) {
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			$pdf->AddPage();
			$details=Db::getInstance()->getInternshipDetails((int)$internship_id);
			$html = file_get_contents(CHEMIN_VUES . 'internship_export.php');
			$html = str_replace("#company_name#",$details['company_name'],$html);
			$html = str_replace("#student_first_name#",$details['first_name_student'],$html);
			$html = str_replace("#student_last_name#",$details['last_name_student'],$html);
			$html = str_replace("#promoter_first_name#",$details['promoter_first_name'],$html);
			$html = str_replace("#promoter_last_name#",$details['promoter_last_name'],$html);
			$html = str_replace("#company_address#",$details['company_address'],$html);
			$html = str_replace("#company_locality#",$details['company_locality'],$html);
			$html = str_replace("#student_email#",$details['student_email'],$html);
			$html = str_replace("#student_phone#",$details['student_phone'],$html);
			$html = str_replace("#promoter_service#",$details['promoter_service'],$html);
			$html = str_replace("#promoter_phone#",$details['promoter_phone'],$html);
			$html = str_replace("#promoter_email#",$details['promoter_email'],$html);
			$html = str_replace("#ic_first_name#",$details['ic_first_name'],$html);
			$html = str_replace("#ic_last_name#",$details['ic_last_name'],$html);
			$html = str_replace("#ic_service#",$details['ic_service'],$html);
			$html = str_replace("#ic_phone_number#",$details['ic_phone_number'],$html);
			$html = str_replace("#ic_email#",$details['ic_email'],$html);
			$html = str_replace("#description#",$details['description'],$html);
			$html = str_replace("#remarks#",$details['remarks'],$html);
			$html = str_replace("#objectives#",$details['objectives'],$html);
			$html = str_replace("#work_environment#",$details['work_environment'],$html);
			$html = str_replace("#internship_origin#",$details['internship_origin'],$html);
			$html = str_replace("#company_zip#",$details['company_zip'],$html);
			$html .= '<style>'.file_get_contents(CHEMIN_VUES.'bootstrap/css/styleExport.css').'</style>';
			$pdf->writeHTML($html, true, 0, true, 0);

			// reset pointer to the last page
			$pdf->lastPage();
			# Un contrôleur se termine en écrivant une vue qui est ici un PDF !
			//Close and output PDF document
		
			$fileName='_export_'.$details['first_name_student'].'_'.$details['last_name_student'].'.pdf';
			$pdf= $pdf->Output($_SERVER['DOCUMENT_ROOT'].date("%d%B%Y").$fileName, 'F');		

		}
			
		header("location: index.php?action=admin&successUploadExport=ok");
		die();

	}
	private static function exportAgreement($internship_id){
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->AddPage();
		$details=Db::getInstance()->getInternshipDetails($internship_id);
		$html = file_get_contents(CHEMIN_VUES . 'internship_agreement.php');

		$html = str_replace("#company_name#",$details['company_name'],$html);
		$html = str_replace("#student_first_name#",$details['first_name_student'],$html);
		$html = str_replace("#student_last_name#",$details['last_name_student'],$html);
		$html = str_replace("#promoter_first_name#",$details['promoter_first_name'],$html);
		$html = str_replace("#promoter_last_name#",$details['promoter_last_name'],$html);
		setlocale (LC_TIME, 'fr_FR.utf8','fra');
		$date=strftime("%d %B %Y");
		$html = str_replace("#date#",$date,$html);
		
		// add css
		$html .= '<style>'.file_get_contents(CHEMIN_VUES.'bootstrap/css/styleConvention.css').'</style>';

		// output the HTML content
		$pdf->writeHTML($html, true, 0, true, 0);

		// reset pointer to the last page
		$pdf->lastPage();


		# Un contrôleur se termine en écrivant une vue qui est ici un PDF !
		//Close and output PDF document
		ob_end_clean();
		$pdf->Output('convention_'.$details['first_name_student'].'_'.$details['last_name_student'].'.pdf', 'I');

	}
}

?>
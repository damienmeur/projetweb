<?php

class LoginController{
	
	public function __controller(){
		
	}
	
	public function run(){
		if(!empty ($_GET['see']) && $_GET['see']=='all'){
			require_once (CHEMIN_VUES . "login.php");
		}

		//import teacher 

	
		$this->setOrUpdateTeachers('models/professeurs.csv');
	
		
		
		
		
		$messError = "";
		
		////////////////Vérification des champs du formulaires entreprise page login///////////////////
		$required = array (
				'entreprise_name',
				'entreprise_address',
				'contact_name',
				'contact_firstName',
				'contact_mail',
				'contact_phone',
				'objectives',
				'work_environment',
				'description',
				'remarks'
		);
		
		// Loop over field names, make sure each one exists and is not empty
		$error = false;
		$success=false;
		foreach ( $required as $field ) {
			if (empty ( $_POST [$field] )) {
				$error = true;
				$success=false;
			}
		}
		
		if (! empty ( $_POST ['submit'] )) {
			if ($error) {
				$messError = "Vous devez remplir tous les champs";
			} else {
		
				//récupère les posts du formulaire et insert dans la db intern_contacts
				Db::getInstance()->insert_contact(htmlentities($_POST['contact_firstName']),($_POST['contact_name']),($_POST['contact_phone']),($_POST['contact_mail']));
		
			//récupère les post du formulaire et insert dans la db companies
 				if(Db::getInstance()->getCompany_id($_POST['entreprise_name'],$_POST['entreprise_address'])===null){
				Db::getInstance()->insert_company(htmlentities($_POST['entreprise_name']),$_POST['entreprise_address'],$_POST['entreprise_locality'],$_POST['entreprise_zip'],$_POST['secretaryship_phone'] );
 				}
				//récupère les posts du formulaire et insert dans la db internships
				$comp_id = Db::getInstance()->getCompany_id($_POST['entreprise_name'],$_POST['entreprise_address']);
				$comp_id =(int) $comp_id;
 				
				//Db::getInstance()->insert_internship(htmlentities($_POST['objectives']),($_POST['remarks']),($_POST['work_environment']),($_POST['description']));
				$contact_id = Db::getInstance()->getContact_id($_POST['contact_firstName'],$_POST['contact_name']);
				$contact_id = (int) $contact_id;
				
				Db::getInstance()->insert_internship($contact_id,$comp_id,htmlentities($_POST['objectives']),($_POST['remarks']),($_POST['work_environment']),($_POST['description']));
				$success=true;
				$messSuccess = "Envoyé avec succès...";
			}
		}
		
		////////////////////////////////////////////////
		
		
		
		
		
		$mess='';
		if(!empty($_POST['email_or_registration_number']) and !empty($_POST['password'])){
			$emailOrRegistrationNumber=htmlentities($_POST['email_or_registration_number']);
			$stud = db::getInstance()->checkFirstLastNameStudent($_POST["email_or_registration_number"]);
			$matches=array();
			preg_match('*@*',$emailOrRegistrationNumber,$matches);
			if(count($matches)==0){
				if(Db::getInstance()->checkStudentAuth($_POST['email_or_registration_number'],$_POST['password'])){
					$_SESSION["studentAuthentification"]=true;
					$_SESSION["login"]=$emailOrRegistrationNumber;
					$_SESSION["first_name"]=$stud['first_name'];
					$_SESSION["last_name"]=$stud['last_name'];
					$_SESSION["email"]=$stud["email"];
					$_SESSION["password"]=sha1($_POST['password']);
					header('location: index.php?action=student');
					die();
				}
				else{
					$mess='Désolé, aucun étudiant avec ce mot de passe ou étudiant absent de la base de donnée';
				}

			}
			else{
				$infoTeachersConnexion=Db::getInstance()->checkTeacherAuth($_POST['email_or_registration_number'],$_POST['password']);
				if($infoTeachersConnexion=="no"){
					header('location: index.php?');
					die();
				}
				$teacherInfo=Db::getInstance()->getTeacherFirstLastName($_POST['email_or_registration_number']);
				$_SESSION['first_name']=$teacherInfo['first_name'];
				$_SESSION['last_name']=$teacherInfo['last_name'];
				$_SESSION["email"]=$emailOrRegistrationNumber;
				$_SESSION['is_admin']=$teacherInfo['is_admin'];
				$_SESSION["login"]=$emailOrRegistrationNumber;
				$_SESSION['password']=sha1(htmlentities($_POST['password']));
				if($_SESSION['is_admin']==0){
					$_SESSION["supervisorAuthentification"]=true;
					header('location: index.php?action=supervisor');
					die();

				}
				elseif($_SESSION['is_admin']==1){
					$_SESSION["supervisorAuthentification"]=true;
					$_SESSION["adminAuthentification"]=true;
					header('location: index.php?action=admin');
					die();
				}
				else{
					$mess='Désolé, aucun professeur avec ce mot de passe ou professeur absent de la base de donnée';
				}
			}
		}
		if(!empty($_POST['email_or_registration_number']) and empty($_POST['password'])){
			$mess='Vous avez oublié d\'encoder le mot de passe';
		}
		if(empty($_POST['email_or_registration_number']) and !empty($_POST['password'])){
			$mess='Vous avez oublié d\'encoder l\'email ou le matricule';
		}
		require_once(CHEMIN_VUES . "login.php");
	}
	
	function setOrUpdateTeachers($csvfile) {
		$teachers=array();
		$teachers=Db::getInstance()->getAllTeachersEvenInactive();
		if($teachers==-1 and file_exists( $csvfile )){
			$success=true;
			$messSuccess="La table teachers de la base de données n'est désormais plus vide, ajout bien effectué";
			
		}				
		if (file_exists ( $csvfile )) {
			$fcontents = file ( $csvfile ); // lire tout le fichier et mettre chaque ligne du fichier dans une case d'un tableau de 0 √† ...
			
			foreach ( $fcontents as $i => $icontent ) {
				if($i!=0){
					preg_match ( '/(.*);(.*);(.*);(.*)/', $icontent, $result );
				
					$isInDataBase=false;
				
					$email_unique_id_to_import=$result[1];
					$last_name=$result[2];
					$first_name=$result[3];
					$is_admin=$result[4];
					if($is_admin{0}=='t') {
						$is_admin=1;
					}
					else $is_admin=0;
					if($teachers!=-1){
						foreach ($teachers as $email_unique_id => $teacher) {
							if(strcmp($email_unique_id,$email_unique_id_to_import)==0){
								$isInDataBase=true;
								if((int)$teacher['is_active']==0){
									Db::getInstance()->setActiveTeacher($email_unique_id_to_import);
								}

							}
						}
					}
					
					if($isInDataBase){
						Db::getInstance()->update_teacher($email_unique_id_to_import, $first_name, $last_name , $is_admin);
					}
					require 'models/config.properties.php';
				
					if(!$isInDataBase) Db::getInstance ()->insert_teacher($email_unique_id_to_import, $first_name, $last_name , $is_admin, $config['teachers_default_pswd']);
					

				}
				
			}

		}

	}
	
}
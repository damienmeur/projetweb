<?php

class StudentController{
	public function __controller(){
		
	}
	
	public function run(){
		$error=false;
		$success=false;
		if(empty($_SESSION['studentAuthentification']) or $_SESSION['studentAuthentification']==false ){
			header('location : index.php?action=login');
			die();
		}
		require 'models/config.properties.php';
		$change_password=false;
		if(sha1($config['students_default_pswd'])==$_SESSION["password"]){
			$change_password=true;
			$error=true;
			$messError="Votre mot de passe est celui par défaut, vous devriez le changer le plus vite possible.";
		}
		
		if(!empty($_GET["change_password"])){
			$messPassword="";
			$change_password=true;
			if(empty($_POST["current_password"]) or empty($_POST["first_password"]) or empty($_POST["second_password"])){
				$error=true;
				$messError="Un des champs n'est pas rempli";
			}
			if(!empty($_POST["current_password"]) and !empty($_POST["first_password"]) and !empty($_POST["second_password"])){
				if(sha1($_POST["current_password"])!=$_SESSION["password"]){
					$error=true;
					$messError="Le mot de passe est incorrect";
				}
				elseif($_POST["first_password"]!=$_POST["second_password"]){
					$error=true;
					$messError="Les mots-de-passe ne correspondent pas";
				}
				else{
					$success=true;
					Db::getInstance()->updatePasswordStudent($_POST["first_password"], $_SESSION["login"]);
					$_SESSION["password"]=sha1(htmlentities($_POST["first_password"]));
					$messSuccess="Le mot de passe a correctement été changé";
				}
			}

		}
		if(!empty($_GET['addInternship'])){
			if($_GET['addInternship']=='success'){
			$success=true;
			$messSuccess="Demande de stage faite";
			}else{
			$error=true;
			$messError="Vous avez déjà un stage!";
					
			}
			
		}
		
		
		$internship_state =Db::getInstance()->get_internship_state(Db::getInstance()->getInternshipId($_SESSION['login']));
		
		$internships = Db::getInstance()->select_company_table();
		
		$config=require 'models/config.properties.php';
		require_once(CHEMIN_VUES . "student.php");
	}
	
}
-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 12 Mai 2016 à 20:23
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bddn_gestion_stages`
--

-- --------------------------------------------------------

--
-- Structure de la table `companies`
--

CREATE TABLE `companies` (
  `company_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `zip` varchar(6) NOT NULL,
  `locality` varchar(100) NOT NULL,
  `secretaryship_phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `companies`
--

INSERT INTO `companies` (`company_id`, `name`, `address`, `zip`, `locality`, `secretaryship_phone`) VALUES
(87, 'Apple', 'One Infinite Loop', '/', 'Cupertino', '/'),
(88, 'BridgeWater', 'Rue de Bailly', '1100', 'Waterloo', '0317403154'),
(89, 'Microsoft', 'Avenue des Lys', '/', 'Californie', '/'),
(90, 'Linux', '16, rue Linus Torvalds', '102543', 'Woluwee-les-enrs', 'a');

-- --------------------------------------------------------

--
-- Structure de la table `internships`
--

CREATE TABLE `internships` (
  `internship_id` int(11) NOT NULL,
  `registration_id` int(11) DEFAULT NULL,
  `first_validator_email` varchar(100) DEFAULT NULL,
  `second_validator_email` varchar(100) DEFAULT NULL,
  `supervisor_email` varchar(100) DEFAULT NULL,
  `promoter_id` int(11) DEFAULT NULL,
  `person_to_contact` int(11) DEFAULT NULL,
  `internship_state` varchar(15) NOT NULL,
  `objectives` varchar(500) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `work_environment` varchar(500) DEFAULT NULL,
  `intership_origin` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `internships`
--

INSERT INTO `internships` (`internship_id`, `registration_id`, `first_validator_email`, `second_validator_email`, `supervisor_email`, `promoter_id`, `person_to_contact`, `internship_state`, `objectives`, `company_id`, `description`, `remarks`, `work_environment`, `intership_origin`) VALUES
(54, 14476, 'gregory.seront@vinci.be', 'jeanluc.collinet@ipl.be', NULL, NULL, 104, 'complete', '/', 87, '															Petite équipe chaleureuse   ', '/', '															Connaissance en python et C++ demandées', '												Demande de stagiaire													'),
(55, NULL, 'jeanluc.collinet@ipl.be', NULL, NULL, NULL, 105, 'soumis', 'Testing sur diff&eacute;rentes applications', 88, 'Start up, open space', 'Equipe familiale', 'Java', NULL),
(56, 4337, 'gregory.seront@vinci.be', 'jeanluc.collinet@ipl.be', NULL, NULL, 106, 'attribue', 'D&eacute;veloppement applications', 89, '					Bonne humeur dans l''ensemble de la société ', '/', '					Swift - Python - C', '					/					'),
(57, 3826, NULL, NULL, NULL, 108, 107, 'soumisEtPris', '/', 90, '																																																Aider à programmer le kernel du nouveau Linux         ', '/', '																																															bash, java, git, latex, html, css, php, assembleur, sql', '																																																J''ai le bras long (je connais Annick par mon beau-frère).																																															');

-- --------------------------------------------------------

--
-- Structure de la table `intern_contacts`
--

CREATE TABLE `intern_contacts` (
  `intern_contact_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `service` varchar(50) DEFAULT NULL,
  `function` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `intern_contacts`
--

INSERT INTO `intern_contacts` (`intern_contact_id`, `first_name`, `last_name`, `email`, `phone_number`, `service`, `function`) VALUES
(104, 'Gerard', 'Lanvin', 'gerardLanvin@gmail.fr', '/', 'IRH', '/'),
(105, 'Luc', 'Dujardin', 'dujardinLuc@hotmail.com', '0783.32.56.54', NULL, NULL),
(106, 'Kevin', 'Smith', 'smith@hotmail.com', '/', 'IRH', '/'),
(107, 'Annick', 'Humalotainne', 'annick-hummalotain@caramail.ca', '0255564181', 'IRH', 'gestionnaire stagiaires externes'),
(108, 'Linus', 'Torvals', 'linus-torvald208@opensource.org', '047556855', 'comptabilité', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `students`
--

CREATE TABLE `students` (
  `registration_id` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `pswd_sha1` varchar(50) NOT NULL,
  `male_female` char(1) DEFAULT NULL,
  `avatar_img_path` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `students`
--

INSERT INTO `students` (`registration_id`, `is_active`, `first_name`, `last_name`, `email`, `phone_number`, `pswd_sha1`, `male_female`, `avatar_img_path`) VALUES
(3826, 1, 'Charles', 'DEMAERSCHALK', 'charles.demaerschalk@student.vinci.be\r', '0475694510', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(4337, 1, 'Kevin', 'BAVAY', 'kevin.bavay@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(4338, 1, 'Loïck', 'BRUNFAUT', 'loick.brunfaut@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(4339, 1, 'Malik', 'AZAOUM', 'malik.azaoum@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(4341, 1, 'Laurent', 'BATSLÉ', 'laurent.batsle@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(4426, 1, 'Mathias', 'OPSOMER', 'mathias.opsomer@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(4429, 1, 'Gaëtan', 'NAVEZ', 'gaetan.navez@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(4448, 1, 'Redouane', 'SAOUTI', 'redouane.saouti@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(4452, 1, 'Mathieu', 'STEENPUT', 'mathieu.steenput@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(4454, 1, 'Jérémy', 'VAN DRIESSCHE', 'jeremy.vandriessche@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(5250, 1, 'Kevin', 'MESTDAGH', 'kevin.mestdagh@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(7315, 1, 'Quoc Dat', 'NGUYEN', 'quocdat.nguyen@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(8014, 1, 'Kevin', 'MAANEN', 'kevin.maanen@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(8271, 1, 'Dorian', 'RICCI', 'dorian.ricci@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(8412, 1, 'Raphael', 'HACHA', 'raphael.hacha@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(8811, 1, 'Roland', 'BAILLY', 'roland.bailly@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(8812, 1, 'Jeremy', 'VANDER AUWERA', 'jeremy.vanderauwera@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(8818, 1, 'Maxime', 'DE MAUBEUGE', 'maxime.demaubeuge@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(8826, 1, 'Soulaïman', 'AFRIKH', 'soulaiman.afrikh@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(8828, 1, 'Giordano', 'AGNELLO', 'giordano.agnello@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(8921, 1, 'Alexandre', 'BRASSEUR', 'alexandre.brasseur@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(9118, 1, 'Kaltrinë', 'KABASHI', 'kaltrine.kabashi@student.vinci.be\r', 'c', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(9918, 1, 'Milenko', 'VORKAPIC', 'milenko.vorkapic@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(10180, 1, 'Laetitia', 'GILLIEAUX', 'laetitia.gillieaux@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(10466, 1, 'Younes', 'KH'' LEEH', 'younes.khleeh@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(11094, 1, 'Antoine', 'COLET', 'antoine.colet@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(11637, 1, 'Gary', 'LOVISETTO', 'gary.lovisetto@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12276, 1, 'Quentin', 'ARTEAGA DAVILA', 'quentin.arteaga@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12283, 1, 'Mathieu', 'FLAMAND', 'mathieu.flamand@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12327, 1, 'Arthur', 'LEMOINE', 'arthur.lemoine@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12329, 1, 'Huseyin', 'TEKTAS', 'huseyin.tektas@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12362, 1, 'Nathan', 'RASPE', 'nathan.raspe@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12373, 1, 'Thibault', 'VANWERSCH', 'thibault.vanwersch@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12466, 1, 'Przemyslaw', 'GASINSKI', 'przemyslaw.gasinski@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12668, 1, 'Michal', 'BRONISZEWSKI', 'michal.broniszewski@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12795, 1, 'Kevin', 'MATHY', 'kevin.mathy@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12820, 1, 'Gaëtan', 'ANSOTTE', 'gaetan.ansotte@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12886, 1, 'Huseyin', 'ERYÖRÜK', 'huseyin.eryoruk@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12974, 1, 'Romain', 'GILLES', 'romain.gilles@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(12987, 1, 'Nicolas', 'VERNAILLEN', 'nicolas.vernaillen@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(13144, 1, 'Thibaud', 'STEVELINCK', 'thibaud.stevelinck@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(13206, 1, 'Sylvain', 'DEVROEDE', 'sylvain.devroede@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(13668, 1, 'Mathieu', 'DESCANTONS de MONTBLANC', 'mathieu.descantons@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(13833, 1, 'Dardan', 'PREBREZA', 'dardan.prebreza@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(13945, 1, 'Loïc', 'DELVAUX', 'loic.delvaux@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(13998, 1, 'Rémi', 'DEGUIDE', 'remi.deguide@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(14032, 1, 'Javier', 'LETHÉ', 'javier.lethe@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(14126, 1, 'Corentin', 'BADOT-BERTRAND', 'corentin.badotbertrand@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(14410, 1, 'Piotr', 'WASILEWSKI', 'piotr.wasilewski@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(14413, 1, 'Arnaud', 'KANDALA MUSANYA', 'arnaud.kandala@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(14476, 1, 'Corentin', 'DANDOY', 'corentin.dandoy@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(14583, 1, 'Mikaël', 'DOMINGUEZ FERNANDEZ', 'mikael.dominguez@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(14694, 1, 'Matteo', 'TAROLI', 'matteo.taroli@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(14707, 1, 'Guylian', 'COX', 'guylian.cox@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(15123, 1, 'David', 'FERNANDEZ GRANDE', 'david.fernandez@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(15125, 1, 'Laurent', 'BULENS', 'laurent.bulens@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(17949, 1, 'Philippe', 'GIUGE', 'philippe.giuge@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(18409, 1, 'Alexandre', 'GIELEN', 'alexandre.gielen@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL),
(18782, 1, 'Fabian', 'GERMEAU', 'fabian.germeau@student.vinci.be\r', '', '7d7606a1f8e34b4a74400af4acef71e7460e5576', '', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `teachers`
--

CREATE TABLE `teachers` (
  `email_unique_id` varchar(50) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `pswd_sha1` varchar(50) NOT NULL,
  `avatar_img_path` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `teachers`
--

INSERT INTO `teachers` (`email_unique_id`, `is_active`, `first_name`, `last_name`, `is_admin`, `pswd_sha1`, `avatar_img_path`) VALUES
('bernard.frank@vinci.be', 1, 'Bernard', 'Frank', 0, '9cf95dacd226dcf43da376cdb6cbba7035218921', NULL),
('bernard.henriet@vinci.be', 1, 'Bernard', 'Henriet', 1, '9cf95dacd226dcf43da376cdb6cbba7035218921', NULL),
('brigitte.binot@vinci.be', 1, 'Brigitte', 'Binot', 0, '9cf95dacd226dcf43da376cdb6cbba7035218921', NULL),
('brigitte.lehmann@vinci.be', 1, 'Brigitte', 'Lehmann', 0, '9cf95dacd226dcf43da376cdb6cbba7035218921', NULL),
('christophe.damas@vinci.be', 1, 'Christophe', 'Damas', 0, '9cf95dacd226dcf43da376cdb6cbba7035218921', NULL),
('emmeline.leconte@vinci.be', 1, 'Emmeline', 'Leconte', 0, '9cf95dacd226dcf43da376cdb6cbba7035218921', NULL),
('gregory.seront@vinci.be', 1, 'Gregory', 'Seront', 1, '9cf95dacd226dcf43da376cdb6cbba7035218921', NULL),
('jeanluc.collinet@ipl.be', 1, 'Jean-Luc', 'Collinet', 1, '9cf95dacd226dcf43da376cdb6cbba7035218921', NULL),
('olivier.choquet@vinci.be', 1, 'Olivier', 'Choquet', 0, '9cf95dacd226dcf43da376cdb6cbba7035218921', NULL),
('stephanie.ferneeuw@vinci.be', 1, 'Stéphanie', 'Ferneeuw', 0, '9cf95dacd226dcf43da376cdb6cbba7035218921', NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`company_id`);

--
-- Index pour la table `internships`
--
ALTER TABLE `internships`
  ADD PRIMARY KEY (`internship_id`),
  ADD KEY `internship_id` (`internship_id`),
  ADD KEY `registration_id` (`registration_id`),
  ADD KEY `first_validator_email` (`first_validator_email`),
  ADD KEY `second_validator_email` (`second_validator_email`),
  ADD KEY `supervisor_email` (`supervisor_email`),
  ADD KEY `promoter_id` (`promoter_id`),
  ADD KEY `person_to_contact` (`person_to_contact`),
  ADD KEY `company_id` (`company_id`);

--
-- Index pour la table `intern_contacts`
--
ALTER TABLE `intern_contacts`
  ADD PRIMARY KEY (`intern_contact_id`);

--
-- Index pour la table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`registration_id`),
  ADD KEY `registration_id` (`registration_id`);

--
-- Index pour la table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`email_unique_id`),
  ADD KEY `email_unique_id` (`email_unique_id`),
  ADD KEY `email_unique_id_2` (`email_unique_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `companies`
--
ALTER TABLE `companies`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `internships`
--
ALTER TABLE `internships`
  MODIFY `internship_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `intern_contacts`
--
ALTER TABLE `intern_contacts`
  MODIFY `intern_contact_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `internships`
--
ALTER TABLE `internships`
  ADD CONSTRAINT `internships_ibfk_10` FOREIGN KEY (`registration_id`) REFERENCES `students` (`registration_id`),
  ADD CONSTRAINT `internships_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `companies` (`company_id`),
  ADD CONSTRAINT `internships_ibfk_3` FOREIGN KEY (`first_validator_email`) REFERENCES `teachers` (`email_unique_id`) ON UPDATE SET NULL,
  ADD CONSTRAINT `internships_ibfk_4` FOREIGN KEY (`second_validator_email`) REFERENCES `teachers` (`email_unique_id`),
  ADD CONSTRAINT `internships_ibfk_6` FOREIGN KEY (`promoter_id`) REFERENCES `intern_contacts` (`intern_contact_id`),
  ADD CONSTRAINT `internships_ibfk_7` FOREIGN KEY (`person_to_contact`) REFERENCES `intern_contacts` (`intern_contact_id`),
  ADD CONSTRAINT `internships_ibfk_9` FOREIGN KEY (`supervisor_email`) REFERENCES `teachers` (`email_unique_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

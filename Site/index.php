<?php
	# Variables globales du site
	ob_start();
	session_start();
	$config=require_once('models/config.properties.php');
	define('CHEMIN_VUES','views/');
	
	function  chargerClasse($classe){
		require_once('models/'.$classe.'.class.php');
	}

	spl_autoload_register('chargerClasse');
	# Ecrire ici le header de toutes pages HTML
	
	
	# Tester si une variable GET 'action' est précisée dans l'URL index.php?action=...

	$action = (isset($_GET['action'])) ? htmlentities($_GET['action']) : 'default';
	# Quelle action est demandée ?
	switch($action) {
		case 'exportate':
			require_once('controllers/ExportPDFController.php');	
			$controller = new ExportPDFController();
			break;
		case 'logout':
			require_once('controllers/LogoutController.php');	
			$controller = new logoutController();
			break;	
		case 'supervisor':
			require_once('controllers/SupervisorController.php');	
			$controller = new SupervisorController();
			break;		
		case 'admin':
			require_once('controllers/AdminController.php');	
			$controller = new AdminController();
			break;		
		case 'student':
			require_once('controllers/StudentController.php');	
			$controller = new StudentController();
			break;		
		case 'form':
			require_once('controllers/FormController.php');
			$controller = new FormController();
			break;
		case 'login':
			require_once('controllers/LoginController.php');
			$controller = new LoginController();
			break;
		default: # Par défaut, le contrôleur de l'accueil est sélectionné
			require_once('controllers/LoginController.php');	
			$controller = new LoginController();
			break;
	}
	require_once(CHEMIN_VUES . 'header.php');
	# Exécution du contrôleur correspondant à l'action demandée
	$controller->run();
	# Ecrire ici le footer du site de toutes pages HTML
	require_once(CHEMIN_VUES . 'footer.php');

	ob_end_flush();	
?>
